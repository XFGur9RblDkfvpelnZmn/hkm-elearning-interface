﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;

namespace BCC.BiPersoneel
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,List<string>> dls = Proces.DlsSqlSel("salnum,naam,afdkod,wplekkod,funktie,wochsoll,ma_grp from personeel");
            StreamWriter sw = new StreamWriter(args[0]);
            foreach (KeyValuePair<string, List<string>> kvp in dls)
            {
                try
                {
                    kvp.Value[4] = (Convert.ToDecimal(kvp.Value[4])/60).ToString("0.00");
                }
                catch (Exception ex)
                {
                }
                sw.WriteLine(kvp.Key + ";" + kvp.Value[0] + ";" + kvp.Value[1] + ";" + kvp.Value[2] + ";" + kvp.Value[3] + ";" + kvp.Value[4] + ";" + kvp.Value[5]);
            }
            sw.Close();
            Proces.VerwijderWrkFiles();

        }
    }
}

