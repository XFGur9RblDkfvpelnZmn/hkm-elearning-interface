﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApplication8
{
    public partial class login : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (pass.Text.Length > 0)
            {
                getAanvraagRecord db = new getAanvraagRecord();
                int aantal = 0;
                try
                {
                    aantal = db.checkLogin(pass.Text.Trim().Substring(3));
                }
                catch (Exception ex)
                {
                }

                if (aantal == 1)
                {
                    Response.Redirect("WebForm1.aspx");
                }
                else if (aantal < 1)
                {
                    Response.Write("Er zijn geen gegevens gevonden met deze badge");
                }
                else
                {
                    Response.Write("Corrupte databank: contacteer <a href='mailto:pieter@ccandela.be'>CCandela</a>");
                }
            }

        }


        protected void pass_TextChanged(object sender, EventArgs e)
        {
            if (pass.Text.Length >= 13)
            {
                getAanvraagRecord db = new getAanvraagRecord();
                int aantal = 0;
                try
                {
                    aantal = db.checkLogin(pass.Text);
                }
                catch (Exception ex)
                {
                }

                if (aantal == 1)
                {
                    Response.Redirect("WebForm1.aspx");
                }
                else if (aantal < 1)
                {
                    Response.Write("Er zijn geen gegevens gevonden met deze badge");
                }
                else
                {
                    Response.Write("Corrupte databank: contacteer <a href='mailto:pieter@ccandela.be'>CCandela</a>");
                }
            }
        }
    }
}
