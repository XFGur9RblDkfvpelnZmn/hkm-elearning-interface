﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Globalization;
using System.Threading;

namespace REB.VakantieWetTmp
{
    class Program
    {
        static void Main(string[] args)
        {

            CultureInfo newCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = newCulture;
            //List<List<string>> lls = Proces.LlsCsvToList(new FileInfo(@"C:\Users\pdoo\Documents\Visual Studio 2010\Projects\SoftbrickLib_2\REB.VakantieWetTmp\bin\Debug\opname.CSV"));

            SEQ seq = SEQ.GetByValue(args[0].ToString());
            FileInfo fiPersTmp = new FileInfo(args[2].ToString());
            FileInfo fiPers = Proces.FiJrovz5pr(DateTime.Now, DateTime.Now, NUM.MIN, seq, Convert.ToInt32(args[1].ToString()), MODE.PDETOT2, fiPersTmp, COMPRESSED.YES, TIMEFRAME.TOTAAL, 0);
            String sPers = Proces.SLeesFile(fiPers);
            //String sPers = "";

            List<List<string>> lls = Proces.LlsCsvToList(new FileInfo(Parameter.SSbRoot+"vst\\export\\VakantieOpname.csv"));
            List<List<string>> llsOp = Proces.LlsCsvToList(new FileInfo(Parameter.SSbRoot+"vst\\export\\VakantieRecht.csv"));
            List<int> li = new List<int>();
            li.Add(1);
            Dictionary<string,List<string>> dlsSal = Proces.DlsCsvToDictionary(new FileInfo(Parameter.SSbRoot + "vst\\export\\VakantieSaldo.csv"),li);

            int iSalnum = 0;
            int iNaam = 1;
            int iLocatie = 4;
            int iOpname = 8;
            int iRecht = 9;
            int iVerschil = 10;
            int iIndienst = 2;
            int iUitdienst = 3;
            int iHuidigSaldo = 7;
            int iAantalKolom = 11;

            int iAfdkod = 5;
            int iWplekkod = 6;

            Dictionary<string, List<string>> dsls = new Dictionary<string, List<string>>();
            foreach (List<string> ls in lls)
            {
                if (ls.Count >= 3)
                {
                    if (ls[0] == "000160")
                    {
                    }
                    if (dsls.ContainsKey(ls[0])&&ls[0]!="salnum-")
                    {
                        if (dsls[ls[0]][5] == string.Empty)
                        {
                            dsls[ls[0]][5] = "0";
                        }
                        decimal d = Convert.ToDecimal(dsls[ls[0]][5]);
                        if (ls[5] == string.Empty)
                        {
                            ls[5] = "0";
                        }
                        d += Convert.ToDecimal(ls[5].Replace(',', '.'));
                        dsls[ls[0]][5] = d.ToString();
                    }
                    else if(ls[0]!="salnum-")
                    {
                        for (int i = 0; i <= 3;i++ )
                        {
                            ls[i] = ls[i];
                        }
                        for (int i = 1; i < iOpname - iLocatie; i++)
                        {
                            ls.Insert(iLocatie + 2, "");
                        }
                        while (ls.Count < iAantalKolom)
                        {
                            ls.Add("");
                        }
                        if (ls[5] != string.Empty)
                        {
                        }
                        ls[5] = ls[5].Replace(',', '.');
                        dsls.Add(ls[0], ls);
                    }
                }
            }
            //List<List<string>> llsOp = Proces.LlsCsvToList(new FileInfo(@"C:\Users\pdoo\Documents\Visual Studio 2010\Projects\SoftbrickLib_2\REB.VakantieWetTmp\bin\Debug\opbouw.CSV"));
            List<List<string>> llsOutput = new List<List<string>>();
            List<string> ls2 = new List<string>();
            while (ls2.Count < iAantalKolom)
            {
                ls2.Add("");
            }
            ls2[iSalnum] = "Salnum";
            ls2[iNaam] = "Naam";
            ls2[iLocatie] = "Locatie";
            ls2[iOpname] = "Opname";
            ls2[iRecht] = "Wettelijk Berekend Saldo (31/12)";
            ls2[iVerschil] = "Op te nemen voor 30/06";
            ls2[iIndienst] = "Geldig van";
            ls2[iUitdienst] = "Geldig t/m";
            ls2[iHuidigSaldo] = "Huidig saldo";
            ls2[iAfdkod] = "Regio";
            ls2[iWplekkod] = "Afdeling";

            llsOutput.Add(ls2);
            foreach (List<string> ls in llsOp)
            {
                if (ls.Count >= 6)
                {
                    if (dsls.ContainsKey(ls[1]) && ls[1] != "salnum-")
                    {

                        if (ls[5].Trim() == string.Empty)
                        {
                            ls[5] = "0";
                        }
                        
                        dsls[ls[1]][iRecht] = ls[7];
                        
                        if (dsls[ls[1]][iOpname] == string.Empty)
                        {
                            dsls[ls[1]][iOpname] = "0";
                        }
                        if (ls[1] == "003181")
                        {
                        }
                        if (dsls[ls[1]][5] == string.Empty)
                        {
                            dsls[ls[1]][5] = "0";
                        }
                        if (ls[7] == string.Empty)
                        {
                            ls[7] = "0";
                        }
                        dsls[ls[1]][iVerschil] = (Convert.ToDecimal(ls[7].Replace(',','.')) - Convert.ToDecimal(dsls[ls[1]][5].Replace(',','.'))).ToString().Replace(',','.');
                        
                        //dsls[ls[1]][3] = (Convert.ToDecimal(dsls[ls[1]][3]) - Convert.ToDecimal(dsls[ls[1]][3]) % 1).ToString("0") + ":" + ((Math.Abs(Convert.ToDecimal(dsls[ls[1]][3]) % 1) * 60)).ToString("00");
                        //dsls[ls[1]][4] = (Convert.ToDecimal(dsls[ls[1]][4]) - Convert.ToDecimal(dsls[ls[1]][4]) % 1).ToString("0") + ":" + ((Math.Abs(Convert.ToDecimal(dsls[ls[1]][4]) % 1) * 60)).ToString("00");
                        //dsls[ls[1]][5] = (Convert.ToDecimal(dsls[ls[1]][5]) - Convert.ToDecimal(dsls[ls[1]][5]) % 1).ToString("0") + ":" + ((Math.Abs(Convert.ToDecimal(dsls[ls[1]][5]) % 1) * 60)).ToString("00");
                        dsls[ls[1]][iOpname] = dsls[ls[1]][5].Replace(',', '.');
                        dsls[ls[1]][iRecht] = dsls[ls[1]][iRecht].Replace(',', '.');
                        //dsls[ls[1]][iAfdkod] = dsls[ls[1]][6].Replace(',', '.');
                        //dsls[ls[1]][iWplekkod] = dsls[ls[1]][7].Replace(',', '.');
                        if (dlsSal.ContainsKey(ls[1]+";"))
                        {
                            dsls[ls[1]][iHuidigSaldo] = dlsSal[ls[1]+";"][8].Replace(',', '.');
                            dsls[ls[1]][iAfdkod] = dlsSal[ls[1] + ";"][5].Replace(',', '.');
                            dsls[ls[1]][iWplekkod] = dlsSal[ls[1] + ";"][6].Replace(',', '.');
                        }

                        if (sPers.Contains(dsls[ls[1]][iSalnum]) && dsls[ls[1]][iSalnum].Length>0)
                            llsOutput.Add(dsls[ls[1]]);
                    }
                }
                
            }
            
            Proces.FiListToGrid("VakantieBerekening", llsOutput);
            Proces.VerwijderWrkFiles();
        }
    }
}

