﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;

namespace REB.VakantieWetData
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dtStart = Proces.dtStringToDate("20120101");
            DateTime dtEind = Proces.dtStringToDate("20130630");

            Parameter.SMandant = "dis";

            int iLijstNrOp = 36;
            int iLijstNr = 35;

            List<string> lsArgs = new List<string>();
            lsArgs.Add("-s");

            FileInfo fiSqlsel = Proces.FiSqlsel("afdkod from afdeling", lsArgs);
            FileInfo fiOp = Proces.FiJrovz5pr(dtStart, dtEind, NUM.NN, SEQ.AFDNUM, iLijstNrOp, MODE.LETOT, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 049);
            fiOp = Proces.FiCsvrap(fiOp, TIMEFORMAT.HOURS_DECIMAL);
            fiSqlsel = Proces.FiSqlsel("badnum from personeel", lsArgs);
            FileInfo fi = Proces.FiJrovz5pr(new DateTime(2012, 1, 1), new DateTime(2012, 12, 31), NUM.NN, SEQ.BADNUM, iLijstNr, MODE.PDETOT2, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 048);
            fi = Proces.FiCsvrap(fi, TIMEFORMAT.HOURS_DECIMAL);

            FileInfo fiSal = Proces.FiJrovz5pr(DateTime.Now, DateTime.Now, NUM.NN, SEQ.BADNUM, iLijstNr, MODE.PDETOT2, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 048);
            fiSal = Proces.FiCsvrap(fiSal, TIMEFORMAT.HOURS_DECIMAL);

            File.Copy(fi.FullName, Parameter.SSbRoot + "vst\\export\\VakantieRecht.csv",true);
            File.Copy(fiOp.FullName, Parameter.SSbRoot + "vst\\export\\VakantieOpname.csv",true);
            File.Copy(fiSal.FullName, Parameter.SSbRoot + "vst\\export\\VakantieSaldo.csv", true);

            Parameter.SMandant = "dis";



            lsArgs = new List<string>();
            lsArgs.Add("-s");

            fiSqlsel = Proces.FiSqlsel("afdkod from afdeling", lsArgs);
            fiOp = Proces.FiJrovz5pr(dtStart, dtEind, NUM.NN, SEQ.AFDNUM, iLijstNrOp, MODE.LETOT, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 049);
            fiOp = Proces.FiCsvrap(fiOp, TIMEFORMAT.HOURS_DECIMAL);
            fiSqlsel = Proces.FiSqlsel("badnum from personeel", lsArgs);
            fi = Proces.FiJrovz5pr(new DateTime(2012, 1, 1), new DateTime(2012, 12, 31), NUM.NN, SEQ.BADNUM, iLijstNr, MODE.PDETOT2, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 048);
            fi = Proces.FiCsvrap(fi, TIMEFORMAT.HOURS_DECIMAL);

            fiSal = Proces.FiJrovz5pr(DateTime.Now, DateTime.Now, NUM.NN, SEQ.BADNUM, iLijstNr, MODE.PDETOT2, fiSqlsel, COMPRESSED.YES, TIMEFRAME.TOTAAL, 048);
            fiSal = Proces.FiCsvrap(fiSal, TIMEFORMAT.HOURS_DECIMAL);

            StreamWriter sw = new StreamWriter(Parameter.SSbRoot + "vst\\export\\VakantieRecht.csv", true);
            sw.WriteLine(Proces.SLeesFile(fi));
            sw.Close();

            //sw = new StreamWriter(Parameter.SSbRoot + "vst\\export\\VakantieOpname.csv", true);
            //sw.WriteLine(Proces.SLeesFile(fiOp));
            //sw.Close();

            sw = new StreamWriter(Parameter.SSbRoot + "vst\\export\\VakantieSaldo.csv", true);
            sw.WriteLine(Proces.SLeesFile(fiSal));
            sw.Close();



            Proces.VerwijderWrkFiles();
        }
    }
}
