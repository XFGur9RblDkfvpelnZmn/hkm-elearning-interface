﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Text.RegularExpressions;


namespace BCC.VerwijderKolom
{
    class Program
    {
        /// <summary>
        /// arg 1= input file
        /// arg 2= output file
        /// arg 3= weg te laten kolom
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            if (args.Length == 3)
            {
                try
                {
                    FileInfo fi = new FileInfo(args[0]);
                    StreamReader sr = new StreamReader(fi.FullName);
                    string sInhoud = sr.ReadToEnd();
                    sr.Close();
                    StreamWriter sw = new StreamWriter(args[1]);

                    foreach (string sLijn in Regex.Split(sInhoud,Environment.NewLine))
                    {
                        string[] sVeld = sLijn.Split(';');
                        for (int i = 0; i < sVeld.Length; i++)
                        {
                            if(i!=Convert.ToInt32(args[2]))
                            {
                                sw.Write(sVeld[i] + ";");

                            }
                            
                        }
                        sw.WriteLine();
                    }
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
                
            }
            else
            {
                Log.Verwerking("Onjuist aantal argumenten");
            }
        }
    }
}
