﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace CEA.DC.HerstelImport
{
    class Program
    {
        static void Main(string[] args)
        {
            Algemeen.Parameter.SMandant = "DIS";
            FileInfo fiInput = new FileInfo(args[0]);
            string sGewerktUC = args[1];
            List<List<string>> llsInput = Algemeen.Proces.LlsCsvToList(fiInput);

            foreach (List<string> ls in llsInput)
            {
                if (ls.Count >= 7)
                {
                    DateTime dtDate = Algemeen.Proces.dtStringToDate(ls[1]);
                    string sSalnum = ls[0];
                    String sBadnum = Algemeen.Proces.SLeesFile(Algemeen.Proces.FiSqlsel("badnum from personeel where salnum = '" + sSalnum + "'")).Trim();
                    String sPopulatie = Algemeen.Proces.SLeesFile(Algemeen.Proces.FiSqlsel("pers_bereich from personeel where salnum = '" + sSalnum + "'")).Trim();
                    int iStartTijdDeel1 = iHhoplaTijdNaarMinuten(ls[3]);
                    int iStartTijdDeel2 = iHhoplaTijdNaarMinuten(ls[6]);
                    if (ls[2] == sGewerktUC)
                    {
                        maakGewerkt(sBadnum, dtDate, iStartTijdDeel1);
                    }
                    if (ls[5] == sGewerktUC)
                    {
                        maakGewerkt(sBadnum, dtDate, iStartTijdDeel2);
                    }

                    int iEindTijdDeel1 = iHhoplaTijdNaarMinuten(ls[4]);
                    int iEindTijdDeel2 = iHhoplaTijdNaarMinuten(ls[7]);
                    int iTotaalDeel1 = iEindTijdDeel1 - iStartTijdDeel1;
                    int iTotaalDeel2 = iEindTijdDeel2 - iStartTijdDeel2;
                    int iTotaalMinuten = iTotaalDeel1 + iTotaalDeel2;

                    int iTotaalPauze = 0;
                    if (sPopulatie == "B")
                    {
                        if (iTotaalMinuten > 240)
                        {
                            iTotaalPauze += 15;
                        }
                        if (iTotaalMinuten > 360)
                        {
                            iTotaalPauze += 30;
                        }
                        if (iTotaalMinuten > 525)
                        {
                            iTotaalPauze += 15;
                        }
                    }
                    else if (sPopulatie == "A")
                    {
                        if (iTotaalMinuten > 239)
                        {
                            iTotaalPauze += 15;
                        }
                        if (iTotaalMinuten > 314)
                        {
                            iTotaalPauze += 30;
                        }
                        if (iTotaalMinuten > 479)
                        {
                            iTotaalPauze += 15;
                        }
                    }
                    else
                    {
                        if (iTotaalMinuten >= 360)
                        {
                            iTotaalPauze += 30;
                        }
                    }
                    decimal dPercDeel1 = Convert.ToDecimal(iTotaalDeel1) / Convert.ToDecimal(iTotaalMinuten);
                    decimal dPercDeel2 = Convert.ToDecimal(iTotaalDeel2) / Convert.ToDecimal(iTotaalMinuten);

                    int iPauzeDeel1 = Convert.ToInt32(dPercDeel1 * Convert.ToDecimal(iTotaalPauze));
                    int iPauzeDeel2 = Convert.ToInt32(dPercDeel2 * Convert.ToDecimal(iTotaalPauze));

                    int iPauzeAfrond1 = iPauzeDeel1 % 15;
                    iPauzeDeel2 += iPauzeAfrond1;
                    iPauzeDeel1 -= iPauzeAfrond1;

                    int iPauzeAfrond2 = iPauzeDeel2 % 15;
                    iPauzeDeel1 += iPauzeAfrond2;
                    iPauzeDeel2 -= iPauzeAfrond2;

                    if (iPauzeDeel1 > 0)
                        importPauze(sBadnum, dtDate, iStartTijdDeel1, iPauzeDeel1);
                    if (iPauzeDeel2 > 0)
                        importPauze(sBadnum, dtDate, iStartTijdDeel2, iPauzeDeel2);
                }
            }
        }

        static int iHhoplaTijdNaarMinuten(string sHhoplaTijd)
        {

            int iHhoplaTijd = 0;
            int iUren = 0;
            int iMinuten = 0;
            int iReturn = 0;
            if (Int32.TryParse(sHhoplaTijd, out iHhoplaTijd))
            {
                iUren = iHhoplaTijd / 100;
                iMinuten = iHhoplaTijd % 100;
                iReturn = iUren * 60 + iMinuten;
            }
            return iReturn;
        }
        static void maakGewerkt(string sBadnum, DateTime dtDate, int iStartTijd)
        {
            List<string> lsParam = new List<string>();
            lsParam.Add("-i");
            string sActkod = Algemeen.Proces.SLeesFile(Algemeen.Proces.FiSqlsel("actkod from proj_regi where van_date = '" + dtDate.ToString("yyMMdd") + "' and badnum = '" + sBadnum + "' and van_time = '" + iStartTijd.ToString("0000") + "' and actkod = '08'", lsParam));
            sActkod = sActkod.Replace(",actkod,08", ",actkod,");
            FileInfo fiDbimp = Algemeen.Proces.fiSchrijfFile(sActkod);
            Algemeen.Proces p = new Algemeen.Proces("dbimport \"" + fiDbimp.FullName + "\"");
        }

        static void importPauze(string sBadnum, DateTime dtDate, int iStartTijd,int iPauze)
        {
            List<string> lsParam = new List<string>();
            lsParam.Add("-i");
            
            string sSqlSel = Algemeen.Proces.SLeesFile(Algemeen.Proces.FiSqlsel("filler,time from proj_regi where van_date = '" + dtDate.ToString("yyMMdd") + "' and badnum = '" + sBadnum + "' and van_time = '" + iStartTijd.ToString("0000") + "'", lsParam));
            string[] sRegel = Regex.Split(sSqlSel, Environment.NewLine);
            if (sRegel.Length >= 2)
            {
                sRegel[0] = sRegel[0].Replace(",filler,", ",filler," + iPauze.ToString("00"));
                string[] asTime = sRegel[1].Split(',');
                int iTmp = 0;
                if (Int32.TryParse(asTime[3], out iTmp))
                    asTime[3] = (iTmp - iPauze).ToString("0000");
                sRegel[1] = asTime[0] + ',' + asTime[1] + ',' + asTime[2] + ',' + asTime[3];
            }
            string sDbImp = sRegel[0] + Environment.NewLine + sRegel[1];
            FileInfo fiDbimp = Algemeen.Proces.fiSchrijfFile(sDbImp);
            Algemeen.Proces p = new Algemeen.Proces("dbimport \"" + fiDbimp.FullName + "\"");
        }
    }
}
