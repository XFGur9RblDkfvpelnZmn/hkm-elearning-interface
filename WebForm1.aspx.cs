﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class WebForm1 : System.Web.UI.Page
    {
       
            //  CONNECTIE SLUITEN
            protected void closeConnection(SqlConnection conn)
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            //  CONNECTIE OPENEN
            protected SqlConnection createConnection()
            {
                //Data Source=DEV0237\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;
                //Data Source=DEV0237;Initial Catalog=DB;Integrated Security=SSPI
                string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
                SqlConnection conn = new SqlConnection(connstring);
                conn.Open();
                return conn;
            }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<input type='button' value='<' onclick=\"document.getElementById('datum').value = parseInt(document.getElementById('datum').value)-1;getFlashMovie('FlashControl1').geefJaar(document.getElementById('datum').value)\" /><input type='text' id='datum' name='datum' value='2009' onchange='getFlashMovie(\"FlashControl1\").geefJaar(this.value)' /><input type='button' value='>' onclick=\"document.getElementById('datum').value = parseInt(document.getElementById('datum').value)+1;getFlashMovie('FlashControl1').geefJaar(document.getElementById('datum').value)\" />");
            afsluit.Attributes.Add("onclick", "return message()");

            if (Convert.ToInt32(Session["salnum"]) > 0)
            {
                
                SqlConnection conn = createConnection();
                SqlDataReader rdr = null;
                SqlCommand checkBetaald = new SqlCommand("SELECT SUM(saldo) 'betaald' FROM teller WHERE werknemerSalnum = '" + Session["salnum"] + "' AND ucatId IN (SELECT ucatId FROM ucat WHERE type='B')", conn);
                rdr = checkBetaald.ExecuteReader();
                rdr.Read();

                decimal betaald = 0;
                try
                {
                    Convert.ToDecimal(rdr["betaald"]);
                }
                catch (Exception ex)
                {
                }
                rdr.Close();
                closeConnection(conn);

                getAanvraagRecord aanvraag = new getAanvraagRecord();
                ArrayList ucat = new ArrayList();
                ucat = aanvraag.getUcats();
                string inactief;

                

                for (int i = 0; i < ucat.Count; i++)
                {
                    if (i == 0)
                    {
                        if (((getAanvraagRecord.ucat)ucat[0]).geldig == 0)
                        {
                            inactief = "disabled='disabled'";
                        }
                        else
                        {
                            inactief = "";
                            
                        }
                        ucats.InnerHtml += "<input type='hidden' id='ucat' value='' /><input type='radio' id='uc' name='ucat' value='' style='visibility:hidden' /><br /><input style='background-color: rgb(" + ((getAanvraagRecord.ucat)ucat[0]).r + "," + ((getAanvraagRecord.ucat)ucat[0]).g + "," + ((getAanvraagRecord.ucat)ucat[0]).b + ");' type='radio' id='uc" + ((getAanvraagRecord.ucat)ucat[0]).id + "' onclick='document.getElementById(\"ucat\").value=\"" + ((getAanvraagRecord.ucat)ucat[0]).id + "\"' name='ucat' value='" + ((getAanvraagRecord.ucat)ucat[0]).id + "' " + inactief + " />" + ((getAanvraagRecord.ucat)ucat[0]).omschrijving + "<br />";
                    }
                    else
                    {
                        if (((getAanvraagRecord.ucat)ucat[i]).geldig == 0)
                        {
                            inactief = "disabled='disabled'";
                        }
                        else if ((((getAanvraagRecord.ucat)ucat[i]).type == "O") && (betaald > 0))
                        {
                            inactief = "disabled='disabled'";
                        }
                        else
                        {
                            inactief = "";
                            ucats.InnerHtml += "<input type='radio' style='background-color: rgb(" + ((getAanvraagRecord.ucat)ucat[i]).r + "," + ((getAanvraagRecord.ucat)ucat[i]).g + "," + ((getAanvraagRecord.ucat)ucat[i]).b + ");' id='uc" + ((getAanvraagRecord.ucat)ucat[i]).id + "' onclick='document.getElementById(\"change\").value=\"1\";document.getElementById(\"ucat\").value=\"" + ((getAanvraagRecord.ucat)ucat[i]).id + "\"' name='ucat' value='" + ((getAanvraagRecord.ucat)ucat[i]).id + "' " + inactief + " />" + ((getAanvraagRecord.ucat)ucat[i]).omschrijving + "<br />";
                        }
                        
                    }
                }
                if (Convert.ToString(Session["type"]) == "per")
                {
                    Response.Write("<a href='Annuleer.aspx' target='_blank'>Aanvragen annuleren</a>");
                }
                else if (Convert.ToString(Session["type"]) == "adm")
                {
                    Response.Write("<a href='koppelChef.aspx' target='_blank'>Chef/Afd Koppelen</a> | ");
                    Response.Write("<a href='Ucat.aspx' target='_blank'>Urencategorieën aanpassen</a> | ");
                    Response.Write("<a href='Administratie.aspx' target='_blank'>Administratie gebruikers</a> | ");
                    Response.Write("<a href='Annuleer2.aspx' target='_blank'>Aanvragen annuleren</a> <br />");
                }
                else if (Convert.ToString(Session["type"]) == "chf")
                {
                    Response.Write("<a href='Chefmenu.aspx' target='_blank'>Chefmenu</a> | ");
                    
                }
                Response.Write("<a href='Overzicht.aspx' target='_blank'>Overzicht afwezigheden</a>");
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void afsluit_Click1(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("login.aspx");
        }

 
    }
}
