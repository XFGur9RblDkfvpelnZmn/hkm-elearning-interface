﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ucat.aspx.cs" Inherits="WebApplication8.Ucat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" ShowFooter="True"
            AutoGenerateColumns="False" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" 
            GridLines="None">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" 
                    SortExpression="id" />
                <asp:BoundField DataField="omschrijving" HeaderText="omschrijving" 
                    SortExpression="omschrijving" />
                <asp:BoundField DataField="min" HeaderText="min" SortExpression="min" />
                <asp:CheckBoxField DataField="geldig" HeaderText="geldig" 
                    SortExpression="geldig" />
                <asp:BoundField DataField="type" HeaderText="type" SortExpression="type" />
                <asp:BoundField DataField="R" HeaderText="R" SortExpression="R" />
                <asp:BoundField DataField="G" HeaderText="G" SortExpression="G" />
                <asp:BoundField DataField="B" HeaderText="B" SortExpression="B" />
            </Columns>
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" 
            DeleteCommand="DELETE FROM [ucat] WHERE [id] = @original_id AND [omschrijving] = @original_omschrijving AND [min] = @original_min AND [geldig] = @original_geldig AND [type] = @original_type AND [R] = @original_R AND [G] = @original_G AND [B] = @original_B" 
            InsertCommand="INSERT INTO [ucat] ([id], [omschrijving], [min], [geldig], [type], [R], [G], [B]) VALUES (@id, @omschrijving, @min, @geldig, @type, @R, @G, @B)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT * FROM [ucat]" 
            UpdateCommand="UPDATE [ucat] SET [omschrijving] = @omschrijving, [min] = @min, [geldig] = @geldig, [type] = @type, [R] = @R, [G] = @G, [B] = @B WHERE [id] = @original_id AND [omschrijving] = @original_omschrijving AND [min] = @original_min AND [geldig] = @original_geldig AND [type] = @original_type AND [R] = @original_R AND [G] = @original_G AND [B] = @original_B">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_omschrijving" Type="String" />
                <asp:Parameter Name="original_min" Type="Int32" />
                <asp:Parameter Name="original_geldig" Type="Boolean" />
                <asp:Parameter Name="original_type" Type="String" />
                <asp:Parameter Name="original_R" Type="Decimal" />
                <asp:Parameter Name="original_G" Type="Decimal" />
                <asp:Parameter Name="original_B" Type="Decimal" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="omschrijving" Type="String" />
                <asp:Parameter Name="min" Type="Int32" />
                <asp:Parameter Name="geldig" Type="Boolean" />
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="R" Type="Decimal" />
                <asp:Parameter Name="G" Type="Decimal" />
                <asp:Parameter Name="B" Type="Decimal" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_omschrijving" Type="String" />
                <asp:Parameter Name="original_min" Type="Int32" />
                <asp:Parameter Name="original_geldig" Type="Boolean" />
                <asp:Parameter Name="original_type" Type="String" />
                <asp:Parameter Name="original_R" Type="Decimal" />
                <asp:Parameter Name="original_G" Type="Decimal" />
                <asp:Parameter Name="original_B" Type="Decimal" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="id" Type="Int32" />
                <asp:Parameter Name="omschrijving" Type="String" />
                <asp:Parameter Name="min" Type="Int32" />
                <asp:Parameter Name="geldig" Type="Boolean" />
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="R" Type="Decimal" />
                <asp:Parameter Name="G" Type="Decimal" />
                <asp:Parameter Name="B" Type="Decimal" />
            </InsertParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
