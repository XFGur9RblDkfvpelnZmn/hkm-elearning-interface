﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Text.RegularExpressions;

namespace HKM.OverzichtKloktijd
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Parameter.SMandant = "ned";
            foreach (string arg in args)
            {
                Log.Verwerking(arg);
            }
            DateTime dtStart = Proces.dtStringToDate(args[0]);
            DateTime dtEind = Proces.dtStringToDate(args[1]);
            
            NUM num = NUM.GetByValue(args[2]);
            SEQ seq = SEQ.GetByValue(args[3]);
            MODE mode = MODE.PDETOT2;
            FileInfo fi = new FileInfo(args[6]);
            //fi.CopyTo(@"c:\softbrick\log\mlajze.txt");
            COMPRESSED c = COMPRESSED.YES;
            TIMEFRAME t = TIMEFRAME.TOTAAL;
            TIMEFORMAT tf = TIMEFORMAT.HOURS;
            List<List<string>> lls = Proces.LlsJrovz5pr(dtStart, dtEind, num, seq, 999, mode, fi, c, t, 0, tf);
            */
            DateTime dtStart = Proces.dtStringToDate(args[0]);
            DateTime dtEind = Proces.dtStringToDate(args[1]);
            //FileInfo fi;
            //string  sBadnums = string.Empty;
            //try
            //{
            //     fi = new FileInfo(args[2]);
            //     sBadnums = Proces.SLeesFile(fi);
            //}
            //catch(Exception ex)
            //{
            //    Log.Exception(ex);
            //    Console.WriteLine("Fout bij openen file");
            //}
            


            //foreach (string sBadnum in Regex.Split(sBadnums,Environment.NewLine))
            //{
                    
                    List<string> lsArgs = new List<string>();
                    lsArgs.Add("-d;");
                    //FileInfo fiSql = Proces.FiSqlsel("salnum, afdkod, wplekkod from personeel where badnum = '" + sBadnum.Trim() + "'",lsArgs);
                    //string sData = Proces.SLeesFile(fiSql).Trim().Replace(Environment.NewLine, "");
                    //if(sData.Length >= 1)
                    //{
                    //    string sSalnum = sData.Split(';')[0].Trim();
                    //    string sAfdkod = sData.Split(';')[1].Trim();
                    //    string sWplekkod = sData.Split(';')[2].Trim();
                        
                        string sProjRegi = Proces.SLeesFile(Proces.FiSqlsel("van_date, van_time, time, filler, afdkod,wplekkod,badnum from proj_regi where van_date >= '"+dtStart.ToString("yyMMdd")+"' and van_date <= '"+dtEind.ToString("yyMMdd")+"' and plan = '1' and actkod = ''",lsArgs));
                        foreach (string sLijn in Regex.Split(sProjRegi, Environment.NewLine))
                        {
                            if (sLijn.Split(';').Length > 5)
                            {
                                string sBadnum = sLijn.Split(';')[6].Trim();
                                FileInfo fiSql = Proces.FiSqlsel("salnum, afdkod, wplekkod from personeel where badnum = '" + sBadnum.Trim() + "'",lsArgs);
                                string sData = Proces.SLeesFile(fiSql).Trim().Replace(Environment.NewLine, "");
                                if (sData.Length >= 1)
                                {
                                    string sSalnum = sData.Split(';')[0].Trim();
                                    string sAfdkod = sData.Split(';')[1].Trim();
                                    string sWplekkod = sData.Split(';')[2].Trim();

                                    DateTime dtStartTijd = Proces.dtStringToDate(sLijn.Split(';')[0].Trim());

                                    #region berekenStarttijd
                                    int iVanTijd = 0;
                                    try
                                    {
                                        iVanTijd = Convert.ToInt32(sLijn.Split(';')[1].Trim());
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Exception(ex);
                                    }
                                    int iVanTijdUur = iVanTijd / 60;
                                    int iVanTijdMin = iVanTijd % 60;
                                    dtStartTijd = dtStartTijd.AddHours(iVanTijdUur);
                                    dtStartTijd = dtStartTijd.AddMinutes(iVanTijdMin);
                                    #endregion
                                    #region berekenTijd
                                    int iTijdMin = 0;
                                    try
                                    {
                                        iTijdMin = Convert.ToInt32(sLijn.Split(';')[2].Trim());
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Exception(ex);
                                    }
                                    TimeSpan tsTijd = new TimeSpan(0, iTijdMin, 0);
                                    #endregion
                                    #region berekenPauze
                                    int iPauzeMin = 0;
                                    try
                                    {
                                        iPauzeMin = Convert.ToInt32(sLijn.Split(';')[3].Trim());
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Exception(ex);
                                    }
                                    TimeSpan tsPauze = new TimeSpan(0, iPauzeMin, 0);
                                    #endregion

                                    if (sLijn.Split(';')[4].Trim().Length > 0)
                                    {
                                        sAfdkod = sLijn.Split(';')[4].Trim();
                                    }
                                    if (sLijn.Split(';')[5].Trim().Length > 0)
                                    {
                                        sWplekkod = sLijn.Split(';')[5].Trim();
                                    }
                                    PlanRecord pr = new PlanRecord(sBadnum, sSalnum, dtStartTijd, tsTijd, tsPauze, sAfdkod, sWplekkod);
                                }
                            }
                        }
                        

            //        }
                
            //}

            StreamWriter sw = new StreamWriter(args[2]);
            foreach (PlanRecord pr in PlanRecord.lprLijst)
            {
                DateTime dtEindTijd = pr.DtStartTijd+pr.TsTijd+pr.TsPauze;
                sw.WriteLine(pr.SBadnum + ";" + pr.SSalnum + ";" + pr.SAfdkod + ";" + pr.SWplekkod + ";" + pr.DtStartTijd.ToString("yyyyMMdd") + ";" + pr.DtStartTijd.ToString("HH:mm") + ";" + dtEindTijd.ToString("HH:mm") + ";" + pr.TsTijd.ToString().Substring(0, 5) + ";" + pr.TsPauze.ToString().Substring(0, 5) + ";");
            }
            sw.Close();

            Proces.VerwijderWrkFiles();
        }
    }
}
