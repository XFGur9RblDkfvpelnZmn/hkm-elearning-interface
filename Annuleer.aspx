﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Annuleer.aspx.cs" Inherits="WebApplication8.Annuleer" %>

<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.WebControls" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" language="javascript">
    function DeleteConfirmation()
    {
    if (confirm("Are you sure," 
                "you want to delete selected records ?")==true)
       return true;
    else
       return false;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:DropDownList ID="ddSalnum" DataSourceID="dsSalnum" 
    AutoPostBack="true" DataValueField="salnum" runat="server" Width="130px" Font-Size="11px" 
    AppendDataBoundItems="true">
        <asp:ListItem Text="All" Value="%"></asp:ListItem>
    </asp:DropDownList>
    <asp:SqlDataSource ID="dsSalnum" runat="server" 
    ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" SelectCommand="SELECT 
    DISTINCT salnum from [werknemer]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" 
        FilterExpression="Convert([werknemerSalnum], 'System.String') like '{0}'"
        SelectCommand="SELECT MIN(aanvraagDetail.datum) AS 'beginSort', MAX(aanvraagDetail.datum) AS 'eindSort',aanvraag.id AS 'AANVRAAG', aanvraag.werknemerSalnum, aanvraag.ucatId, SUBSTRING(MIN(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 1, 4) AS Begindag, SUBSTRING(MAX(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 1, 4) AS Einddag, CASE WHEN status = 0 THEN 'IN AANVRAAG' WHEN status = 1 THEN 'ANNULATIE' WHEN status = 2 THEN 'AFKEURING' WHEN status= 3 THEN 'IN WACHT' WHEN status=4 THEN 'GOEDGEKEURD' END AS statusText, status, CASE WHEN aanvraagDetail.type = 1 THEN 'HELE DAG' WHEN aanvraagDetail.type = 2 THEN 'VOORMIDDAG' WHEN aanvraagDetail.type = 3 THEN 'NAMIDDAG' WHEN aanvraagDetail.type = 0 THEN 'VAN/TOT' END AS type, SUBSTRING(aanvraagDetail.van, 1, 2) + ':' + SUBSTRING(aanvraagDetail.van, 3, 2) AS 'van', SUBSTRING(aanvraagDetail.tot, 1, 2) + ':' + SUBSTRING(aanvraagDetail.tot, 3, 2) AS 'tot' FROM aanvraag INNER JOIN aanvraagDetail ON aanvraag.id = aanvraagDetail.aanvraagId INNER JOIN werknemer ON aanvraag.werknemerSalnum = werknemer.salnum GROUP BY aanvraagDetail.aanvraagId, aanvraag.werknemerSalnum, aanvraag.ucatId, aanvraagDetail.status, aanvraagDetail.type, aanvraagDetail.van, aanvraagDetail.tot, aanvraag.id" 
        UpdateCommand="UPDATE aanvraagDetail SET status = @status WHERE aanvraagId = @AANVRAAG">
        <UpdateParameters>
            <asp:Parameter Name="status" />
            <asp:Parameter Name="AANVRAAG" />
        </UpdateParameters>
        <FilterParameters>
            <asp:ControlParameter Name="salnum" ControlID="ddSalnum" 
                PropertyName="SelectedValue" />
        </FilterParameters>
    </asp:SqlDataSource>
    <asp:GridView Width="100%" ID="GridView1" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="AANVRAAG" 
        DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" 
        onselectedindexchanged="GridView1_SelectedIndexChanged" AllowPaging="True">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="AANVRAAG" HeaderText="AANVRAAG" 
                InsertVisible="False" ReadOnly="True" SortExpression="AANVRAAG" />
            <asp:BoundField DataField="werknemerSalnum" HeaderText="werknemerSalnum" 
                ReadOnly="True" SortExpression="werknemerSalnum" />
            <asp:BoundField DataField="ucatId" HeaderText="ucatId" ReadOnly="True" 
                SortExpression="ucatId" />
            <asp:BoundField DataField="Begindag" HeaderText="Begindag" ReadOnly="True" 
                SortExpression="beginSort" />
            <asp:BoundField DataField="Einddag" HeaderText="Einddag" ReadOnly="True" 
                SortExpression="eindSort" />
            <asp:TemplateField HeaderText="status" SortExpression="status">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" SelectedValue='<%# Bind("status") %>' runat="server" DataValueField="status"   DataTextField="status"  AppendDataBoundItems="true">
                        
                        <asp:ListItem Value="0" Text="in aanvraag" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="1" Text="annulatie" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="2" Text="afkeuring" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="3" Text="in wacht" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="4" Text="goedkeuring" Enabled="true"></asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("statusText") %>' ReadOnly="true"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="type" HeaderText="type" ReadOnly="True" 
                SortExpression="type" />
            <asp:BoundField DataField="van" HeaderText="van" ReadOnly="True" 
                SortExpression="van" />
            <asp:BoundField DataField="tot" HeaderText="tot" ReadOnly="True" 
                SortExpression="tot" />
        </Columns>
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle HorizontalAlign="Left" BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    </form>
</body>
</html>
