﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace VER.SalarisInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Algemeen.Parameter.SMandant = "BEL";
            Log.Verwerking("Start Salarisverwerking");
            string sWerkgeverNr = Proces.SLeesFile(Proces.FiSqlsel("clientnum from msyspar")).Trim();
            string sGWTCode = Proces.SLeesFile(Proces.FiSqlsel("compnr50 from msyspar")).Trim();
            
            string sNewSalinf = string.Empty;
            //lees salarisinterface in objecten
            foreach(string sLijn in Regex.Split(Proces.SLeesFile(new FileInfo(Parameter.SSbRoot+"log\\expblox1.txt")),Environment.NewLine))
            {
                try
                {
                    string sLooncomp = Proces.SLeesFile(Proces.FiSqlsel("catoms[8] from urencategorie where catkod = '" + sLijn.Substring(22, 2) + "'")).Trim();
                    if (sLijn.Substring(20, 4) == sGWTCode)
                    {
                        sLooncomp = sGWTCode;
                    }
                    else if (sLijn.Substring(20, 4) == "0000")
                    {
                        sLooncomp = "0000";
                    }
                    if (sLooncomp.Length == 4)
                    {
                        Record r = new Record(sLijn.Substring(7, 7), sWerkgeverNr, sLijn.Substring(14, 6), sLooncomp, sLijn.Substring(24, 4));
                    }
                    
                }
                catch (Exception ex)
                {
                    if (sLijn.Trim().Length > 0)
                    {
                        Log.Verwerking("regel niet verwerkt: " + sLijn);
                        Log.Exception(ex);
                    }
                }
            }

            //verwerk meeruren
            foreach (Record r in Record.lrRecords)
            {
                if ((r.sLooncode == "7018") && (r.dtDatum.DayOfWeek == DayOfWeek.Sunday))
                {
                    Record.lrNewRecords.AddRange(r.omboekMeerUren());
                }
            }
            Record.lrRecords.AddRange(Record.lrNewRecords);

            foreach (Record r in Record.lrRecords)
            {
                r.checkDubbel(Record.lrRecords);
                if (((r.sLooncode == "7010") || (r.sLooncode == "0000")) && (r.iUren == 0) && (r.bestaatRecordZelfdeDag()))
                {
                    r.sSalnum = "weg";
                }
            }
            Record.lrRecords.Sort();
            FileInfo fi = new FileInfo(Parameter.SSbRoot + "log\\expblox_vw.txt");
            TextWriter tw = new StreamWriter(fi.FullName);
            foreach (Record r in Record.lrRecords)
            {
                if (r.sSalnum != "weg")
                {
                    tw.Write(r.sWerkgeverNr+r.sSalnum+"K"+r.dtDatum.ToString("yyyyMMdd")+r.sLooncode+r.iUren.ToString("0000")+Environment.NewLine);
                    
                }
            }
            
            tw.Close();
            Proces.Tc_lp(fi);
            Log.Verwerking("Einde Salarisinterface");
        }
        
    }
}
