﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication8
{
    public class OverzichtRecord
    {
        public static List<OverzichtRecord> lorLijst = new List<OverzichtRecord>();
        public String sDatum;
        public decimal van = new decimal();
        public decimal tot = new decimal();
        public decimal[] tijd = new decimal[30];

        public OverzichtRecord(string sDatum)
        {
            this.sDatum = sDatum;
        }
    }
}