﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class Roulement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  OPEN SQL CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //Response.Write(Convert.ToString(minuten));
            SqlCommand roulement = new SqlCommand("SELECT dag FROM planning WHERE werknemerSalnum = '" + Session["salnum"] + "' AND minuten='1' AND dag >'" + Request["jaar"]+"0000' and dag <= '"+Request["jaar"]+"1231'", conn);
            rdr = roulement.ExecuteReader();
            string dag;
            
            while (rdr.Read())
            {
                dag = Convert.ToString(rdr["dag"]);
                DateTime datum = new DateTime(Convert.ToInt32(dag.Substring(0, 4)),Convert.ToInt32(dag.Substring(4, 2)),Convert.ToInt32(dag.Substring(6, 2)));
                DateTime check = new DateTime(Convert.ToInt32(dag.Substring(0, 4)), 02, 28);
                check = check.AddDays(1);
                int dayOfYear = datum.DayOfYear;
                if ((check.Day == 01)&&(datum.Month>2))
                {
                    dayOfYear++;
                }


                //if (datum.Year == DateTime.Now.Year)
                {
                    Response.Write(dayOfYear + ";");
                }
            }


            conn.Close();

            //  HAAL FEESTDAGEN OP EN GEEF WEER
            getAanvraagRecord aanvraag = new getAanvraagRecord();
            ArrayList line = new ArrayList();
            line = aanvraag.getFeestdag();
            for (int i = 0; i < line.Count; i++)
            {
                dag = ((getAanvraagRecord.record)line[i]).beginDag;
                DateTime datum = new DateTime(Convert.ToInt32(dag.Substring(0, 4)), Convert.ToInt32(dag.Substring(4, 2)), Convert.ToInt32(dag.Substring(6, 2)));
                DateTime check = new DateTime(Convert.ToInt32(dag.Substring(0, 4)), 02, 28);
                check = check.AddDays(1);
                int dayOfYear = datum.DayOfYear;
                if ((check.Day == 01) && (datum.Month > 2))
                {
                    dayOfYear++;
                }
                if (datum.Year == Convert.ToInt32(Request["jaar"]))
                {
                    Response.Write(dayOfYear + ";");
                }
            }
        }
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
    }
}
