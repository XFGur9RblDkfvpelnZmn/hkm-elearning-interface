﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class opslaanExtrAanvr : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            //  OPEN SQL CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;
            int id;
            char[] delimiters = new char[] { '/', '-', '.', ':', '+', '=', '_', ' ', '*', '&' };
            int type=0;
            if (Convert.ToInt32(Request["wanneer"]) == 2)
            {
                type = 2;
            }
            else if (Convert.ToInt32(Request["wanneer"]) == 3)
            {
                type = 3;
            }
            else
            {
                type = 0;
            }
            try
            {
                if ((Convert.ToString(Request["ucat"]).Length > 0) && ((type != 0) || ((Convert.ToString(Request["van"]).Length > 4) && (Convert.ToString(Request["tot"]).Length > 4))))
                {


                    string vanu = "";
                    string totu = "";
                    decimal van = 0, tot = 0;
                    if (Convert.ToInt32(type) == 0)
                    {

                        try
                        {
                            vanu = Convert.ToString(Request["van"]).Substring(0, 2) + Convert.ToString(Request["van"]).Substring(3, 2);
                            totu = Convert.ToString(Request["tot"]).Substring(0, 2) + Convert.ToString(Request["tot"]).Substring(3, 2);
                            int hulp = Convert.ToInt32(Convert.ToString(vanu).Substring(2, 2));
                            hulp = hulp / 3 * 5;
                            van = Convert.ToDecimal(Convert.ToString(vanu).Substring(0, 2) + "." + hulp.ToString("00"));
                            hulp = Convert.ToInt32(Convert.ToString(totu).Substring(2, 2));
                            hulp = hulp / 3 * 5;
                            tot = Convert.ToDecimal(Convert.ToString(totu).Substring(0, 2) + "." + hulp.ToString("00"));
                            //Response.Write(Convert.ToString(Convert.ToString(vanu).Substring(0, 2) + "," + Convert.ToString(vanu).Substring(2, 2) + "m"));

                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex);
                        }
                    }
                    string[] dagen = Request["txtTot"].Split(';');

                    //  MAAK DATETIME AAN VOOR BEGIN- EN EINDDAG
                    string[] split = Request["txtVan"].Split(delimiters);
                    if (split[2].Length < 3)
                    {
                        split[2] = "20" + split[2];
                    }
                    DateTime dag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));
                    split = Request["txtTot"].Split(delimiters);
                    DateTime eindDag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));
                    DateTime tel = dag;
                    decimal saldoNodig = 0m;
                    string error = "";
                    while (tel <= eindDag)
                    {
                        SqlCommand vorigeAanvr = new SqlCommand("SELECT van,tot,type FROM aanvraagDetail WHERE aanvraagId = '" + Convert.ToString(Request["aanvrId"]) + "'", conn);
                        rdr = vorigeAanvr.ExecuteReader();
                        rdr.Read();
                        decimal vorigVan = 0m;
                        decimal vorigTot = 0m;

                        if (Convert.ToInt32(rdr["type"]) == 0)
                        {
                            vorigVan = Convert.ToInt32(rdr["van"]);
                            vorigTot = Convert.ToInt32(rdr["tot"]);
                        }

                        rdr.Close();

                        if (vorigVan != 0m)
                        {
                            int hulp = Convert.ToInt32(vorigVan.ToString("0000").Substring(2, 2));
                            hulp = hulp / 3 * 5;
                            vorigVan = Convert.ToDecimal(vorigVan.ToString("0000").Substring(0, 2) + "." + hulp.ToString("00"));
                            hulp = Convert.ToInt32(vorigTot.ToString("0000").Substring(2, 2));
                            hulp = hulp / 3 * 5;
                            vorigTot = Convert.ToDecimal(vorigTot.ToString("0000").Substring(0, 2) + "." + hulp.ToString("00"));
                        }
                        SqlCommand checkPlanning = new SqlCommand("SELECT minuten 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag = '" + tel.Year.ToString("0000") + tel.Month.ToString("00") + tel.Day.ToString("00") + "'", conn);
                        //Response.Write("SELECT minuten 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag = '" + tel.Year.ToString("0000") + tel.Month.ToString("00") + tel.Day.ToString("00") + "'");
                        //Response.Write("SELECT SUM(minuten) 'minuten' FROM planning,werknemer WHERE werknemerRecnum=recordnr AND salnum='" + Session["salnum"] + "' AND dag >= '" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "' AND dag <= '" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "'");
                        rdr = checkPlanning.ExecuteReader();
                        rdr.Read();
                        if (rdr.HasRows)
                        {
                            switch (Convert.ToInt32(type))
                            {
                                case 1:
                                    try
                                    {
                                        saldoNodig += Convert.ToDecimal(rdr["minuten"]) / 60;
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    break;
                                case 2:
                                case 3:

                                    try
                                    {
                                        decimal check = Convert.ToDecimal(rdr["minuten"]) / 60;
                                        if ((check) >= Convert.ToDecimal(8))
                                        {
                                            saldoNodig += Convert.ToDecimal(rdr["minuten"]) / 120;
                                        }
                                        else
                                        {
                                            Response.Write("Op deze dag(en) kan geen halve dag aangevraagd worden.");
                                            error = "fout";
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                    break;
                                case 0:
                                    try
                                    {
                                        if (((van * 60) == Convert.ToDecimal(rdr["van"])) || ((tot * 60) == (Convert.ToDecimal(rdr["van"]) + Convert.ToDecimal(rdr["minuten"]))) || (van == vorigTot) || (tot == vorigVan))
                                        {
                                            saldoNodig += tot - van;
                                        }
                                        else
                                        {
                                            error = "fout";
                                            Response.Write("Een aanvraag met van- en totuur moet op het begin of einde van je planning liggen.");
                                            Response.Write("<br />Gebruik de terug-knop van je browser om terug te keren naar het vorige scherm.");
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    break;
                            }
                        }
                        rdr.Close();
                        tel = tel.AddDays(1);
                    }


                    //Response.Write(Convert.ToString(minuten));
                    SqlCommand checkTeller = new SqlCommand("SELECT saldo FROM teller WHERE werknemerSalnum = '" + Session["salnum"] + "' AND jaar = '"+tel.Year.ToString("0000")+"' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                    rdr = checkTeller.ExecuteReader();
                    rdr.Read();
                    decimal saldo;
                    if (rdr.HasRows)
                    {
                        saldo = Convert.ToDecimal(rdr["saldo"]);
                    }
                    else
                    {
                        saldo = 0;
                    }
                    rdr.Close();

                    //Response.Write(Convert.ToString(rdr["saldo"]) +"/"+ Convert.ToString(totaal)+"/"+Convert.ToString(minuten)+"#"+Convert.ToString(checkPlanning));
                    if ((saldo >= saldoNodig) && (error.Length < 1))
                    {

                        SqlCommand saldoUpdate = new SqlCommand("UPDATE teller SET saldo=saldo-'" + saldoNodig.ToString("#0.00").Replace(',','.') + "' WHERE werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                        saldoUpdate.ExecuteNonQuery();
                        //Response.Write("UPDATE teller SET saldo=saldo-'" + totaal + "' WHERE werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')");
                        //MAAK NIEUW RECORD AAN IN aanvraag
                        SqlCommand cmd = new SqlCommand("INSERT into aanvraag (werknemerSalnum, ucatId) values ('" + Session["salnum"] + "','" + Request["ucat"] + "')", conn);
                        try
                        {
                            cmd.ExecuteScalar();
                        }
                        finally
                        {
                            //  SELECTEER ID VAN aanvraag
                            SqlCommand getMax = new SqlCommand("SELECT MAX(id) 'id' FROM aanvraag WHERE werknemerSalnum='" + Session["salnum"] + "'", conn);
                            rdr = getMax.ExecuteReader();
                            rdr.Read();
                            id = Convert.ToInt32(rdr["id"]);
                            rdr.Close();
                            int geheel = Convert.ToInt32(Math.Floor(saldoNodig));
                            decimal deel = (saldoNodig - geheel) * 100;
                            deel = deel / 10 * 6;

                            // UPDATE VORIGE AANVRAAG MET EXTRAAANVRAAG
                            SqlCommand updateVorig = new SqlCommand("UPDATE aanvraagDetail SET extraAanvraag = '" + id + "', status='0' WHERE aanvraagId = '" + Request["aanvrId"] + "'", conn);
                            updateVorig.ExecuteNonQuery();

                            //MAAK PDF VOOR CODE 46 & 45
                            SqlCommand selectPers = new SqlCommand("SELECT naam FROM werknemer WHERE salnum='" + Session["salnum"] + "'", conn);
                            rdr = selectPers.ExecuteReader();
                            rdr.Read();
                            if (Convert.ToInt32(Request["ucat"]) == 45)
                            {
                                pdf45.createDoc(rdr["naam"].ToString(), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(type), Request["vanu"], Request["totu"], geheel.ToString("00") + ":" + deel.ToString("00"));
                            }
                            else if (Convert.ToInt32(Request["ucat"]) == 46)
                            {
                                pdf.createDoc(rdr["naam"].ToString(), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(type), Request["vanu"], Request["totu"], geheel.ToString("00") + ":" + deel.ToString("00"));
                            }
                            rdr.Close();

                            //  VOOR ELKE TUSSENLIGGENDE DAG
                            while (dag <= eindDag)
                            {
                                //  MAAK EEN DETAILRECORD AAN
                                SqlCommand insertDetail = new SqlCommand("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('00000000','" + vanu + "','" + totu + "','" + Convert.ToString(type) + "','" + id + "')", conn);
                                //Response.Write("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + type + "','" + id + "')");
                                insertDetail.ExecuteNonQuery();

                                //  GA NAAR VOLGENDE DAG
                                dag = dag.AddDays(1);
                            }



                            //  SLUIT CONNECTIE
                            closeConnection(conn);
                        }
                        Response.Write("Uw aanvraag is correct geregistreerd. Klik <a href=\"javascript: javascript:window.opener=\'x';window.close()\">HIER</a> om het scherm te sluiten.");
                    }
                    else if (error.Length > 0)
                    {
                        rdr.Close();
                    }
                    else
                    {
                        rdr.Close();
                        Response.Write("Uw saldo is te laag voor deze aanvraag.");
                        Response.Write("<br />Gebruik de terug-knop van je browser om terug te keren naar het vorige scherm.");
                    }



                }
                else
                {
                    Response.Write("Gelieve alle velden in te vullen, uw aanvraag is niet verwerkt!");
                    Response.Write("<br />Gebruik de terug-knop van je browser om terug te keren naar het vorige scherm.");
                }
            }
            catch (Exception ex)
            {
                Response.Write("Gelieve alle velden in te vullen, uw aanvraag is niet verwerkt!");
                Response.Write("<br />Gebruik de terug-knop van je browser om terug te keren naar het vorige scherm.");
            }
        }
        //  CONNECTIE SLUITEN
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                conn.Close();
            }
        }

        //  CONNECTIE OPENEN
        protected SqlConnection createConnection()
        {
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            return conn;
        }
    }
}
