﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Algemeen
{
    public class Proces
    {
        #region wrkfiles
        public static List<FileInfo> LfiWrkFiles = new List<FileInfo>();
        public static void VerwijderWrkFiles()
        {

            foreach (FileInfo fi in LfiWrkFiles)
            {
                if (File.Exists(fi.FullName))
                    File.Delete(fi.FullName);
            }
            Log.Verwerking("wrk files verwijderd");
        }
        #endregion

        #region std process
        public Proces(string arg)
        {
            try
            {
                string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");
                if (Parameter.SMandant.Length > 0)
                    tw.WriteLine("SET TC_MANDANT=" + Parameter.SMandant);
                tw.WriteLine(arg);

                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");

                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;

                //start bat file
                proces.Start();
                proces.WaitForExit();
                if (Parameter.SMandant.Length > 0)
                    Log.Verwerking("SET TC_MANDANT=" + Parameter.SMandant+Environment.NewLine+arg);
                else
                    Log.Verwerking(arg);
                //File.Delete(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        public Proces(List<string> args)
        {
            try
            {
                string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");
                if(Parameter.SMandant.Length > 0)
                    tw.WriteLine("SET TC_MANDANT=" + Parameter.SMandant);
                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;
                //start bat file
                proces.Start();
                proces.WaitForExit();
                if (Parameter.SMandant.Length > 0)
                    Log.Verwerking("SET TC_MANDANT=" + Parameter.SMandant);
                foreach (string arg in args)
                    Log.Verwerking(arg);
                //File.Delete(Parameter.SSbRoot + "log\\tmp" + sTimestamp + ".bat");
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        #endregion

        public static void EventLog(string sCode, string sText)
        {
            new Proces(Parameter.SSbRoot + "bin\\clnt_exp event_log " + sCode + " \"" + sText + "\"");
        }

        #region jrovz5pr
        public static List<List<string>> LlsJrovz5pr(DateTime dtVanDat, DateTime dtTotDat, NUM num, SEQ seq, int iLijst, MODE mode, FileInfo fiTmpFile, COMPRESSED compressed, TIMEFRAME timeFrame, int iVisualSql, TIMEFORMAT timeFormat)
        {
            FileInfo fiJrovz5pr = FiJrovz5pr(dtVanDat, dtTotDat, num, seq, iLijst, mode, fiTmpFile, compressed, timeFrame, iVisualSql);
            FileInfo fiCsvRap = FiCsvrap(fiJrovz5pr, timeFormat);
            List<List<String>> llsReturnList = LlsCsvToList(fiCsvRap);
            return llsReturnList;
        }
        public static FileInfo FiJrovz5pr(DateTime dtVanDat, DateTime dtTotDat, NUM num, SEQ seq, int iLijst, MODE mode, FileInfo fiTmpFile, COMPRESSED compressed, TIMEFRAME timeFrame, int iVisualSql)
        {
            string sFileName = Parameter.SSbRoot + "log\\jrovz5pr_" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\jrovz5pr " + dtVanDat.ToString("yyMMdd") + " " + dtTotDat.ToString("yyMMdd") + " "+num.ToString()+" " + seq.ToString() + " " + iLijst.ToString() + " "+mode.ToString()+" " + fiTmpFile.FullName + " "+compressed.ToString()+" " + timeFrame.ToString() + " " + iVisualSql.ToString() + " grd=" + sFileName);
            FileInfo fi = new FileInfo(sFileName);
            LfiWrkFiles.Add(fi);
            return fi;
        }
        #endregion

        public static FileInfo FiCsvrap(FileInfo fiFile, TIMEFORMAT timeFormat)
        {
            string sFileName = Parameter.SSbRoot + "log\\csvrap_" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\csvrap.exe -i" + fiFile.FullName + " -o" + sFileName + " -t"+timeFormat+" -r -h");
            FileInfo fi = new FileInfo(sFileName);
            LfiWrkFiles.Add(fi);
            return fi;
        }

        #region sqlsel
        public static FileInfo FiSqlsel(string sSqlCommando)
        {
            string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string sFileName = Parameter.SSbRoot + "log\\sqlsel" + sTimestamp + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\sqlsel \"" + sSqlCommando + "\" > " + sFileName + " -s");
            FileInfo fi = new FileInfo(sFileName);
            LfiWrkFiles.Add(fi);
            return fi;
        }
        public static FileInfo FiSqlsel(string sSqlCommando, List<string> args)
        {
            string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            string sFileName = Parameter.SSbRoot + "log\\sqlsel" + sTimestamp + ".wrk";
            string sCmd = Parameter.SSbRoot + "bin\\sqlsel \"" + sSqlCommando + "\" > " + sFileName + " -s";
            foreach (string arg in args)
            {
                sCmd += " " + arg;
            }
            new Proces(sCmd);
            FileInfo fi = new FileInfo(sFileName);
            LfiWrkFiles.Add(fi);
            return fi;
        }
        public static Dictionary<string, List<string>> DlsSqlSel(string sSqlCommando)
        {
            List<string> lsArgs = new List<string>();
            lsArgs.Add("-d;");
            FileInfo fi = FiSqlsel(sSqlCommando, lsArgs);
            string sInhoud = SLeesFile(fi);
            Dictionary<string, List<string>> dlsSqlsel = new Dictionary<string, List<string>>();
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                if (sLijn.Trim().Length > 0)
                {
                    List<string> lsRecord = new List<string>();
                    int i = 0;
                    string sKey = string.Empty;
                    foreach (string sData in sLijn.Split(';'))
                    {
                        
                        if (i == 0)
                        {
                            sKey = sData.Trim();
                        }
                        else
                        {
                            lsRecord.Add(sData.Trim());
                        }
                        if (sKey == "003")
                        {
                        }

                        i++;
                    }
                    if (sKey.Trim().Length > 0 && !dlsSqlsel.ContainsKey(sKey))
                        dlsSqlsel.Add(sKey, lsRecord);
                }
            }
            return dlsSqlsel;
        }
        public static Dictionary<string, List<string>> DlsSqlSel(string sSqlCommando, DateTime dtDatum)
        {
            Dictionary<string, List<string>> dlsSqlsel = null;
            try
            {
                List<string> lsArgs = new List<string>();
                lsArgs.Add("-d;");
                lsArgs.Add("-e" + dtDatum.ToString("yyyyMMdd"));
                FileInfo fi = FiSqlsel(sSqlCommando, lsArgs);
                string sInhoud = SLeesFile(fi);
                dlsSqlsel = new Dictionary<string, List<string>>();
                foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
                {
                    if (sLijn.Replace(";", "").Replace(" ", "").Length > 0)
                    {
                        List<string> lsRecord = new List<string>();
                        int i = 0;
                        string sKey = string.Empty;
                        foreach (string sData in sLijn.Split(';'))
                        {
                            if (i == 0)
                            {
                                sKey = sData.Trim();
                            }
                            else
                            {
                                lsRecord.Add(sData.Trim());
                            }
                            i++;
                        }
                        dlsSqlsel.Add(sKey, lsRecord);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Log.Exception(ex);
                Log.Verwerking("Key is niet uniek");
            }
            return dlsSqlsel;
        }
        public static List<string> lsSqlSelHeaders(string sSqlCommando)
        {
            string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            string sFileName = Parameter.SSbRoot + "log\\sqlsel" + sTimestamp + ".wrk";
            string sCmd = Parameter.SSbRoot + "bin\\sqlsel \"" + sSqlCommando + " where 1 = 2\" > " + sFileName + " -d;";

            new Proces(sCmd);
            FileInfo fi = new FileInfo(sFileName);
            LfiWrkFiles.Add(fi);
            string sHeaders = Proces.SLeesFile(fi);
            List<string> lsHeaders = new List<string>();
            foreach (string sHeader in sHeaders.Split(';'))
            {
                lsHeaders.Add(sHeader.Trim().Replace("-", ""));
            }
            return lsHeaders;
        }
        #endregion

        #region list to grid
        public static FileInfo FiListToGrid(string sTitle, List<List<string>> llsInput)
        {
            string sOutput = "{"+Environment.NewLine+"title:"+sTitle+Environment.NewLine+"header:";
            foreach (string s in llsInput[0])
            {
                sOutput += s + "~";
            }
            sOutput += Environment.NewLine + "types:";
            foreach (string s in llsInput[0])
            {
                sOutput += "text~";
            }
            sOutput += Environment.NewLine + "token: jrovz5pr_13" + Environment.NewLine + "grid_col_sel" + Environment.NewLine + "}" + Environment.NewLine;
            
            for(int i = 1; i< llsInput.Count;i++)
            {
                foreach (string s in llsInput[i])
                {
                    sOutput += s + "~";
                }
                sOutput += Environment.NewLine;
            }
            FileInfo fi = fiSchrijfFile(sOutput);
            Gridrap(fi);
            return null;
        }
        public static FileInfo FiListToGridWithFooter(string sTitle, List<List<string>> llsInput,List<decimal> ldFooter)
        {
            string sOutput = "{" + Environment.NewLine + "title:" + sTitle + Environment.NewLine + "header:";
            foreach (string s in llsInput[0])
            {
                sOutput += s + "~";
            }
            sOutput += Environment.NewLine + "types:";
            foreach (string s in llsInput[0])
            {
                sOutput += "text~";
            }
            sOutput += Environment.NewLine + "token: jrovz5pr_13" + Environment.NewLine + "grid_col_sel" + Environment.NewLine + "}" + Environment.NewLine;

            for (int i = 1; i < llsInput.Count; i++)
            {
                foreach (string s in llsInput[i])
                {
                    sOutput += s + "~";
                }
                sOutput += Environment.NewLine;
            }
            sOutput += "{" + Environment.NewLine + "footer0:";
            foreach (decimal d in ldFooter)
            {
                sOutput += d.ToString()+"~";
            }
            sOutput += "}";
            FileInfo fi = fiSchrijfFile(sOutput);
            Gridrap(fi);
            return null;
        }
        #endregion


        public static void Gridrap(FileInfo fiFile)
        {
            new Proces(Parameter.SSbRoot + "bin\\gridrap.exe scr/gridrap.scr \"" + fiFile.FullName + "\" -r ");
        }
        public static void Tc_lp(FileInfo fiFile)
        {
            List<string> lsCmds = new List<string>();
            lsCmds.Add("cd " + Parameter.SSbRoot + "bin\\");
            lsCmds.Add("tc_lp log\\"+fiFile.Name);
            new Proces(lsCmds);
        }

        #region std funcs
        public static DateTime dtStringToDate(string sDatum)
        {

            int iJaar = 0;
            int iMaand = 0;
            int iDag = 0;
            try
            {
                if (sDatum.Length == 6)
                {
                    iJaar = 2000 + Convert.ToInt32(sDatum.Substring(0, 2));
                    iMaand = Convert.ToInt32(sDatum.Substring(2, 2));
                    iDag = Convert.ToInt32(sDatum.Substring(4, 2));
                }
                else if (sDatum.Length == 0)
                {
                    iJaar = 2000;
                    iMaand = 1;
                    iDag = 1;
                }
                else
                {
                    iJaar = Convert.ToInt32(sDatum.Substring(0, 4));
                    iMaand = Convert.ToInt32(sDatum.Substring(4, 2));
                    iDag = Convert.ToInt32(sDatum.Substring(6, 2));
                }
            }
            catch (Exception ex)
            {
                iJaar = 2000;
                iMaand = 1;
                iDag = 1;
                Log.Verwerking("Datumformaat niet correct");
                Log.Exception(ex);
            }
            DateTime dtDatum = new DateTime(iJaar, iMaand, iDag);
            return dtDatum;
        }
        public static List<List<string>> LlsCsvToList(FileInfo fiCsvFile)
        {
            string sInhoud = SLeesFile(fiCsvFile);
            List<List<String>> llsReturnList = new List<List<string>>();
            int i = 0;
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                List<string> lsLijn = new List<string>();
                foreach (string sVeld in sLijn.Split(';'))
                {
                    if (i != 0)
                    {
                        lsLijn.Add(sVeld.Trim().Replace("+", "").Replace(".", ","));
                    }
                    else
                    {
                        lsLijn.Add(sVeld.Trim());
                    }
                }
                llsReturnList.Add(lsLijn);
                i++;
            }
            return llsReturnList;
        }

        public static Dictionary<string,List<string>> DlsCsvToDictionary(FileInfo fiCsvFile, List<int> liKeyColumns)
        {
            string sInhoud = SLeesFile(fiCsvFile);
            Dictionary<string, List<String>> llsReturnList = new Dictionary<string, List<string>>();
            int i = 0;
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                List<string> lsLijn = new List<string>();
                foreach (string sVeld in sLijn.Split(';'))
                {
                    if (i != 0)
                    {
                        lsLijn.Add(sVeld.Trim().Replace("+", "").Replace(".", ","));
                    }
                    else
                    {
                        lsLijn.Add(sVeld.Trim());
                    }
                }
                string sKey = string.Empty;
                foreach (int a in liKeyColumns)
                {
                    if(a<=lsLijn.Count-1)
                        sKey += lsLijn[a]+";";
                }
                if (sKey.Replace(";", "").Trim().Length > 0)
                {
                    if (sKey != "salnum-;")
                    {
                        try
                        {
                            llsReturnList.Add(sKey, lsLijn);
                        }
                        catch(Exception ex)
                        {
                            Log.Exception(ex);
                        }
                    }
                }
                i++;
            }
            return llsReturnList;
        }

        public static FileInfo FiListToCsv(List<List<string>> llsInput)
        {
            string sOutput = string.Empty;
            
            for (int i = 0; i < llsInput.Count; i++)
            {
                foreach (string s in llsInput[i])
                {
                    sOutput += s + ";";
                }
                sOutput += Environment.NewLine;
            }
            FileInfo fi = fiSchrijfFile(sOutput);
            return fi;
        }
        public static string SLeesFile(FileInfo fiFile)
        {
            string sInhoud = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(fiFile.FullName,Encoding.GetEncoding(1252));
                sInhoud = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            return sInhoud;
        }
        public static FileInfo fiSchrijfFile(string sInhoud)
        {
            string sFileName = Parameter.SSbRoot + "log\\file_" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
            try
            {
                StreamWriter sw = new StreamWriter(sFileName);
                sw.Write(sInhoud);
                sw.Close();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            FileInfo fi = new FileInfo(sFileName);
            return fi;

        }
        #endregion

    }
   

}
