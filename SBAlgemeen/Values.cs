﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
namespace Algemeen
{
    /// <summary>
    /// Basis klasse voor enums met waarde
    /// </summary>
    public abstract class EnumBaseType<T> where T : EnumBaseType<T>
    {
        protected static List<T> enumValues = new List<T>();

        public readonly int Key;
        public readonly string Value;

        public EnumBaseType(int key, string value)
        {
            Key = key;
            Value = value;
            enumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return enumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(int key)
        {
            foreach (T t in enumValues)
            {
                if (t.Key == key) return t;
            }
            return null;
        }

        protected static T GetBaseByValue(string value)
        {
            foreach (T t in enumValues)
            {
                if (t.Value == value) return t;
            }
            return null;
        }

        public override string ToString()
        {
            return Value;
        }
    }
    
    #region JROVZ5PR PARAMS

    public class NUM : EnumBaseType<NUM>
    {
        public static readonly NUM NN = new NUM(0, "0");
        public static readonly NUM MIN = new NUM(1, "-");

        public NUM(int key, string value)
            : base(key, value)
        {
        }

        public static ReadOnlyCollection<NUM> GetValues()
        {
            return GetBaseValues();
        }

        public static NUM GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static NUM GetByValue(string value)
        {
            return GetBaseByValue(value);
        }
    }
    public class SEQ : EnumBaseType<SEQ>
    {
        public static readonly SEQ ALL = new SEQ(0, "0");
        public static readonly SEQ BADNUM = new SEQ(1, "1");
        public static readonly SEQ AFDNUM = new SEQ(2, "2");
        public static readonly SEQ SALNUM = new SEQ(3, "3");
        public static readonly SEQ UITZEND = new SEQ(4, "4");
        public static readonly SEQ NAAM = new SEQ(5, "5");
        public static readonly SEQ WPLEKKOD = new SEQ(6, "6");
        public SEQ(int key, string value)
            : base(key, value)
        {
            
        }

        public static ReadOnlyCollection<SEQ> GetValues()
        {
            return GetBaseValues();
        }

        public static SEQ GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static SEQ GetByValue(string value)
        {
            int iTemp = 0;
            try
            {
                iTemp = Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                Log.Verwerking("Fout SEQ formaat");
                Log.Exception(ex);
            }
            value = iTemp.ToString();
            return GetBaseByValue(value);
        }
    }
    public class MODE : EnumBaseType<MODE>
    {
        public static readonly MODE JROVZ5 = new MODE(0, "jrovz5");
        public static readonly MODE PDETOT2 = new MODE(1, "pdetot2");
        public static readonly MODE LETOT = new MODE(2, "letot");
        public static readonly MODE REGU = new MODE(3, "regu");

        public MODE(int key, string value)
            : base(key, value)
        {
        }

        public static ReadOnlyCollection<MODE> GetValues()
        {
            return GetBaseValues();
        }

        public static MODE GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static MODE GetByValue(string value)
        {
            return GetBaseByValue(value);
        }
    }
    public class COMPRESSED : EnumBaseType<COMPRESSED>
    {
        public static readonly COMPRESSED NO = new COMPRESSED(0, "0");
        public static readonly COMPRESSED YES = new COMPRESSED(1, "1");

        public COMPRESSED(int key, string value)
            : base(key, value)
        {
            
        }

        public static ReadOnlyCollection<COMPRESSED> GetValues()
        {
            return GetBaseValues();
        }

        public static COMPRESSED GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static COMPRESSED GetByValue(string value)
        {
            int iTemp = 0;
            try
            {
                iTemp = Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                Log.Verwerking("Fout SEQ formaat");
                Log.Exception(ex);
            }
            value = iTemp.ToString();
            return GetBaseByValue(value);
        }
    }
    public class TIMEFRAME : EnumBaseType<TIMEFRAME>
    {
        public static readonly TIMEFRAME TOTAAL = new TIMEFRAME(0, "0");
        public static readonly TIMEFRAME DAY_BY_DAY = new TIMEFRAME(1, "1");
        public static readonly TIMEFRAME WEEK_BY_WEEK = new TIMEFRAME(2, "2");
        public static readonly TIMEFRAME PERIOD_BY_PERIOD = new TIMEFRAME(3, "3");
        public static readonly TIMEFRAME MONTH_BY_MONTH = new TIMEFRAME(4, "4");

        public TIMEFRAME(int key, string value)
            : base(key, value)
        {
        }

        public static ReadOnlyCollection<TIMEFRAME> GetValues()
        {
            return GetBaseValues();
        }

        public static TIMEFRAME GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static TIMEFRAME GetByValue(string value)
        {
            return GetBaseByValue(value);
        }
    }

    #endregion

    public class TIMEFORMAT : EnumBaseType<TIMEFORMAT>
    {
        public static readonly TIMEFORMAT MINUTES = new TIMEFORMAT(0, "mm");
        public static readonly TIMEFORMAT HOURS = new TIMEFORMAT(1, "hh");
        public static readonly TIMEFORMAT HOURS_MINUTES = new TIMEFORMAT(1, "hhmm");
        public static readonly TIMEFORMAT HOURS_DECIMAL = new TIMEFORMAT(1, "hh00");
        public static readonly TIMEFORMAT DAYS = new TIMEFORMAT(1, "dd");

        public TIMEFORMAT(int key, string value)
            : base(key, value)
        {
        }

        public static ReadOnlyCollection<TIMEFORMAT> GetValues()
        {
            return GetBaseValues();
        }

        public static TIMEFORMAT GetByKey(int key)
        {
            return GetBaseByKey(key);
        }
        public static TIMEFORMAT GetByValue(string value)
        {
            return GetBaseByValue(value);
        }
    }

}
