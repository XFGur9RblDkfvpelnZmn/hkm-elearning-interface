﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HKM.Elearn
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo fiInput = new FileInfo(Properties.Settings.Default.inputFile);
            FileInfo fiOutput = new FileInfo(Properties.Settings.Default.outputFile);
            FileInfo fiMandant = new FileInfo(Properties.Settings.Default.mandantConvFile);
            FileInfo fiDsgroep = new FileInfo(Properties.Settings.Default.dsgroepConvFile);
            FileInfo fiTaal = new FileInfo(Properties.Settings.Default.taalConvFile);
            FileInfo fiHeader = new FileInfo(Properties.Settings.Default.headerFile);
            

            List<List<string>> llsPers = Algemeen.Proces.LlsCsvToList(fiInput);
            List<int> liKey = new List<int>();
            liKey.Add(0);

            Dictionary<string,List<string>> dlsMandant = Algemeen.Proces.DlsCsvToDictionary(fiMandant,liKey);
            Dictionary<string,List<string>> dlsDsgroep = Algemeen.Proces.DlsCsvToDictionary(fiDsgroep,liKey);
            Dictionary<string,List<string>> dlsTaal = Algemeen.Proces.DlsCsvToDictionary(fiTaal,liKey);
            List<List<string>> llsHeader = Algemeen.Proces.LlsCsvToList(fiHeader);

            for (int i = 0; i < llsPers.Count; i++)
            {
                if (i == 0)
                {
                    llsPers[i] = llsHeader[i];
                }
                else if(llsPers[i].Count>=15)
                {
                    if (llsPers[i][3].Trim().Length == 0)
                    {
                        llsPers[i][3] = "-";
                    }
                    if (dlsDsgroep.ContainsKey(llsPers[i][4] + ";"))
                    {
                        llsPers[i][4] = dlsDsgroep[llsPers[i][4] + ";"][2];
                    }
                    if (dlsTaal.ContainsKey(llsPers[i][9] + ";"))
                    {
                        llsPers[i][9] = dlsTaal[llsPers[i][9] + ";"][4];
                    }
                    if (dlsMandant.ContainsKey(llsPers[i][13] + ";"))
                    {
                        llsPers[i][13] = dlsMandant[llsPers[i][13] + ";"][1];
                    }
                    llsPers[i][12] = llsPers[i][12].ToUpper();
                    for (int a = 0; a < llsPers[i].Count; a++)
                    {
                        llsPers[i][a] = llsPers[i][a].Replace('÷', 'ö');
                    }
                    DateTime dtTmp = DateTime.ParseExact(llsPers[i][16].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    if (dtTmp.Year > 2030)
                        dtTmp = new DateTime(2030, dtTmp.Month, dtTmp.Day);
                    llsPers[i][16] = (dtTmp).ToString("dd-MMM-yy");
                    llsPers[i].RemoveAt(18);
                }
            }
            FileInfo fiOutputTmp = Algemeen.Proces.FiListToCsv(llsPers);
            fiOutputTmp.CopyTo(fiOutput.FullName,true);
            Algemeen.Proces.VerwijderWrkFiles();
        }
    }
}
