﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;
namespace WebApplication8
{
    public partial class extrAanvr : System.Web.UI.Page
    {
        //  CONNECTIE SLUITEN
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                conn.Close();
            }
        }

        //  CONNECTIE OPENEN
        protected SqlConnection createConnection()
        {
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            return conn;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            aanvrId.Value = Convert.ToString(Request["id"]);
            
            if (Convert.ToInt32(Session["salnum"]) > 0)
            {
                SqlConnection conn = createConnection();
                SqlDataReader rdr = null;

                SqlCommand getVanTot = new SqlCommand("SELECT datum,type FROM aanvraagDetail WHERE aanvraagId = '" + Convert.ToString(Request["id"]) + "'",conn);
                rdr = getVanTot.ExecuteReader();
                rdr.Read();

                    string datum = Convert.ToString(rdr["datum"]);
                    txtTot.Value = datum.Substring(6, 2) + "/" + datum.Substring(4, 2) + "/" + datum.Substring(0, 4);
                    txtVan.Value = txtTot.Value;

                    if (Convert.ToInt32(rdr["type"]) == 2)
                    {
                        vm.Disabled = true;
                    }
                    else if (Convert.ToInt32(rdr["type"]) == 3)
                    {
                        nm.Disabled = true;
                    }

                rdr.Close();

                
                SqlCommand checkBetaald = new SqlCommand("SELECT SUM(saldo) 'betaald' FROM teller WHERE werknemerSalnum = '" + Session["salnum"] + "' AND ucatId IN (SELECT ucatId FROM ucat WHERE type='B')", conn);
                rdr = checkBetaald.ExecuteReader();
                rdr.Read();

                decimal betaald = 0;
                try
                {
                    Convert.ToDecimal(rdr["betaald"]);
                }
                catch (Exception ex)
                {
                }
                rdr.Close();
                closeConnection(conn);

                getAanvraagRecord aanvraag = new getAanvraagRecord();
                ArrayList ucat = new ArrayList();
                ucat = aanvraag.getUcats();
                string inactief;

                if (((getAanvraagRecord.ucat)ucat[0]).geldig == 0)
                {
                    inactief = "disabled='disabled'";
                }
                else
                {
                    inactief = "";
                }
                ucats.InnerHtml += "<input type='hidden' id='ucat' value='' /><br /><input style='background-color: rgb(" + ((getAanvraagRecord.ucat)ucat[0]).r + "," + ((getAanvraagRecord.ucat)ucat[0]).g + "," + ((getAanvraagRecord.ucat)ucat[0]).b + ");' type='radio' id='uc" + ((getAanvraagRecord.ucat)ucat[0]).id + "' onclick='document.getElementById(\"ucat\").value=\"" + ((getAanvraagRecord.ucat)ucat[0]).id + "\"' name='ucat' value='" + ((getAanvraagRecord.ucat)ucat[0]).id + "' " + inactief + " />" + ((getAanvraagRecord.ucat)ucat[0]).omschrijving + "<br />";

                for (int i = 1; i < ucat.Count; i++)
                {
                    if (((getAanvraagRecord.ucat)ucat[i]).geldig == 0)
                    {
                        inactief = "disabled='disabled'";
                    }
                    else if ((((getAanvraagRecord.ucat)ucat[i]).type == "O") && (betaald > 0))
                    {
                        inactief = "disabled='disabled'";
                    }
                    else
                    {
                        inactief = "";
                    }
                    ucats.InnerHtml += "<input type='radio' style='background-color: rgb(" + ((getAanvraagRecord.ucat)ucat[i]).r + "," + ((getAanvraagRecord.ucat)ucat[i]).g + "," + ((getAanvraagRecord.ucat)ucat[i]).b + ");' id='uc" + ((getAanvraagRecord.ucat)ucat[i]).id + "' onclick='document.getElementById(\"change\").value=\"1\";document.getElementById(\"ucat\").value=\"" + ((getAanvraagRecord.ucat)ucat[i]).id + "\"' name='ucat' value='" + ((getAanvraagRecord.ucat)ucat[i]).id + "' " + inactief + " />" + ((getAanvraagRecord.ucat)ucat[i]).omschrijving + "<br />";

                }
                
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            
        }

    }
}
