﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SB.Algemeen;
using System.Text.RegularExpressions;
namespace WebApplication8
{
    public partial class Overzicht : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sNaam = SB.Algemeen.Proces.SLeesFile(Proces.FiSqlsel("naam, init from personeel where salnum = '" + Session["salnum"] + "'").FullName).Trim();
            //
            if (cdStartDatum.SelectedDate == new DateTime(0001, 01, 01))
            {
                cdStartDatum.SelectedDate = new DateTime(DateTime.Now.Year, 1, 1);
            }
            if (cdEindDatum.SelectedDate == new DateTime(0001, 01, 01))
            {
                cdEindDatum.SelectedDate = new DateTime(DateTime.Now.Year, 12, 31);
                btnRun_Click(null, null);
            }
            try
            {
                this.divNaam.InnerHtml = Session["salnum"].ToString() + " " + sNaam;
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnRun_Click(object sender, EventArgs e)
        {
            this.divResult.InnerHtml = "";
            OverzichtRecord.lorLijst.Clear();
            Dictionary<string, List<string>> dlsUc = SB.Algemeen.Proces.DlsSqlSel("recnr,ucats[0],ucats[1],ucats[2],ucats[3],ucats[4],ucats[5],ucats[6],ucats[7],ucats[8],ucats[9],ucats[10],ucats[11],ucats[12],ucats[13],ucats[14],ucats[15],ucats[16],ucats[17],ucats[18],ucats[19],ucats[20],ucats[21],ucats[22],ucats[23],ucats[24],ucats[25],ucats[26],ucats[27],ucats[28],ucats[29] from pdeovdef where recnr=81");
            Dictionary<string, List<string>> dlsUcOms = SB.Algemeen.Proces.DlsSqlSel("recnr,omschr2[0],omschr2[1],omschr2[2],omschr2[3],omschr2[4],omschr2[5],omschr2[6],omschr2[7],omschr2[8],omschr2[9],omschr2[10],omschr2[11],omschr2[12],omschr2[13],omschr2[14],omschr2[15],omschr2[16],omschr2[17],omschr2[18],omschr2[19],omschr2[20],omschr2[21],omschr2[22],omschr2[23],omschr2[24],omschr2[25],omschr2[26],omschr2[27],omschr2[28],omschr2[29] from pdeovdef where recnr=81");
            Dictionary<string, List<string>> dlsUcPrint = SB.Algemeen.Proces.DlsSqlSel("recnr,print[0],print[1],print[2],print[3],print[4],print[5],print[6],print[7],print[8],print[9],print[10],print[11],print[12],print[13],print[14],print[15],print[16],print[17],print[18],print[19],print[20],print[21],print[22],print[23],print[24],print[25],print[26],print[27],print[28],print[29] from pdeovdef where recnr=81");
            string sBadnum = SB.Algemeen.Proces.SLeesFile(Proces.FiSqlsel("badnum from personeel where salnum = '" + Session["salnum"] + "'").FullName).Trim();
            Dictionary<string, List<string>> dlsPlan = SB.Algemeen.Proces.DlsSqlSel("recnr,actkod,van_date,van_time,time,filler from proj_regi where badnum = '" + sBadnum + "' and van_date >= '" + cdStartDatum.SelectedDate.ToString("yyMMdd") + "' and van_date <= '" + cdEindDatum.SelectedDate.ToString("yyMMdd") + "' and actkod > 0 and actkod < 100");
            this.divResult.InnerHtml = "<table cellspacing=0 cellpadding=3 style=''>";
            this.divResult.InnerHtml += "<tr style='background-color:#507dd2;color:white;'>";
            decimal[] totaal = new decimal[30];
            this.divResult.InnerHtml += "<td style='width:100px;border-right:2px solid lightgrey;'><b>Datum</b></td>";
            this.divResult.InnerHtml += "<td style='width:150px;border-right:2px solid lightgrey;'><b>Van-Tot</b></td>";
            for (int i = 0; i < 30; i++)
            {
                totaal[i] = 0;
                if (dlsUcPrint["81"][i].Trim() == "1")
                {
                    this.divResult.InnerHtml += "<td style='width:100px;border-right:2px solid lightgrey;'><b>";
                    this.divResult.InnerHtml += dlsUcOms["81"][i];
                    this.divResult.InnerHtml += "</b></td>";
                }
            }
            this.divResult.InnerHtml += "</tr>";
            

            foreach (KeyValuePair<string, List<string>> kvp in dlsPlan)
            {
                OverzichtRecord OR = new OverzichtRecord(kvp.Value[1].ToString());
                //this.divResult.InnerHtml += "<tr>";
                //this.divResult.InnerHtml += "<td>";
                //this.divResult.InnerHtml += "</td>";
                for (int i = 0; i < 30; i++)
                {
                    //this.divResult.InnerHtml += "<td>";
                    int b = 0;
                    foreach (char a in dlsUc["81"][i])
                    {
                        if (b.ToString("00") == kvp.Value[0] && a == '1')
                        {
                            decimal van = 0;
                            decimal tot = 0;
                            decimal hoeveel = 0;
                            try
                            {
                                van = Convert.ToDecimal(kvp.Value[2]);
                                tot = Convert.ToDecimal(kvp.Value[2]) + Convert.ToDecimal(kvp.Value[3])+Convert.ToDecimal("0"+kvp.Value[4]);
                                if (kvp.Value[4] != "")
                                {
                                    hoeveel = Convert.ToDecimal(kvp.Value[3]);
                                    

                                    //+ Convert.ToDecimal(kvp.Value[4]
                                }
                                else
                                {
                                    hoeveel = Convert.ToDecimal(kvp.Value[3]);
                                }
                                string sHoeveel2 = SB.Algemeen.Proces.SLeesFile(Proces.FiSqlsel("time from lewkucat where badnum = '" + sBadnum + "' and date = '"+OR.sDatum+"' and ucat = '"+b+"'").FullName).Trim();
                                if (sHoeveel2.Trim() != string.Empty)
                                {
                                    try
                                    {
                                        if(hoeveel >= Convert.ToDecimal(sHoeveel2))
                                        {
                                            hoeveel = Convert.ToDecimal(sHoeveel2);
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            catch
                                (Exception ex)
                            {

                            }
                            OR.van = (van / 60);
                            OR.tot = (tot / 60);
                            OR.tijd[i] = (hoeveel / 60);
                            //this.divResult.InnerHtml += kvp.Value[1] + " | " + (van/60).ToString() + " - " + (tot/60).ToString() + " = " + (hoeveel/60).ToString();
                            totaal[i] += hoeveel/60;
                        }
                        b++;
                    }
                    
                }
                decimal dTemp = 0;
                for (int c = 0; c < 30; c++)
                {
                    dTemp += OR.tijd[c];
                }
                if (dTemp != 0)
                {
                    OverzichtRecord.lorLijst.Add(OR);
                }
                //this.divResult.InnerHtml += "</td>";
                //this.divResult.InnerHtml += "</tr>";
            }
            //this.divResult.InnerHtml += "<tr>";
            string sColor = string.Empty;
            foreach (OverzichtRecord OR in OverzichtRecord.lorLijst.OrderBy(rec => rec.sDatum))
            {
                if (sColor == "#eff3fc")
                {
                    sColor = "#eaeef7";
                }
                else
                {
                    sColor = "#eff3fc";
                }
                this.divResult.InnerHtml += "<tr style='background-color:" + sColor + ";'>";
                this.divResult.InnerHtml += "<td style='border-right:2px solid lightgrey;'>";
                this.divResult.InnerHtml += OR.sDatum.Substring(4,2)+"/"+OR.sDatum.Substring(2,2)+"/20"+OR.sDatum.Substring(0,2);
                this.divResult.InnerHtml += "</td>";
                this.divResult.InnerHtml += "<td style='border-right:2px solid lightgrey;'>";
                if (OR.van != 0)
                {
                    this.divResult.InnerHtml += Math.Floor(OR.van).ToString("00") + "u" + ((OR.van % 1) * 60).ToString("00") + " - " + Math.Floor(OR.tot).ToString("00") + "u" + ((OR.tot % 1) * 60).ToString("00");
                }
                this.divResult.InnerHtml += "</td>";
                for (int i = 0; i < 30; i++)
                {
                    if (dlsUcPrint["81"][i].Trim() == "1")
                    {
                        this.divResult.InnerHtml += "<td style='border-right:2px solid lightgrey;'>";
                        if (OR.tijd[i] != 0)
                        {
                            this.divResult.InnerHtml += Math.Round(OR.tijd[i],2);
                        }
                        this.divResult.InnerHtml += "</td>";
                    }
                   
                }
                this.divResult.InnerHtml += "</tr>";
            }
            this.divResult.InnerHtml += "<tr style='background-color:#eaeef7'><td style='border-right:2px solid lightgrey;'></td><td style='border-right:2px solid lightgrey;'><b>Totaal</b></td>";
            for (int i = 0; i < 30; i++)
            {
                if (dlsUcPrint["81"][i].Trim() == "1")
                {
                    this.divResult.InnerHtml += "<td style='border-right:2px solid lightgrey;'>";
                    this.divResult.InnerHtml += Math.Round(totaal[i],2).ToString();
                    this.divResult.InnerHtml += "</td>";
                }
            }
            this.divResult.InnerHtml += "</tr>";
            this.divResult.InnerHtml += "</table>";
            Proces.VerwijderWrkFiles();
        }
    }
}