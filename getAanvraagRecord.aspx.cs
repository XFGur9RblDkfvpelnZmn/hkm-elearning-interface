﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;


namespace WebApplication8
{
    public partial class getAanvraagRecord : System.Web.UI.Page
    {
        //  DEFINIEER STRUCTS
        public struct record
        {
            public String beginDag, eindDag, van, tot, type, id, status, ucat,kleur;
        }
        public struct ucat
        {
            public int id, geldig, min,r,g,b;
            public String omschrijving,type;
            
        }

        //  HAAL FEESTDAGEN OP
        public ArrayList getFeestdag()
        {
            //  MAAK CONNECTIE MET DB
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  DEFINIEER VARS
            String beginDag, eindDag;
            ArrayList records = new ArrayList();
            record newRecord = new record();

            try
            {
                //  VOER SQL CMD UIT
                SqlCommand cmd = new SqlCommand("select datum from feestdag", conn);
                rdr = cmd.ExecuteReader();

                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    //  HERSCHIK DATUMS
                    beginDag = Convert.ToString(rdr["datum"]);
                    //beginDag = beginDag.Substring(6, 2) + "/" + beginDag.Substring(4, 2) + "/" + beginDag.Substring(0, 4);

                    eindDag = Convert.ToString(rdr["datum"]);
                    //eindDag = eindDag.Substring(6, 2) + "/" + eindDag.Substring(4, 2) + "/" + eindDag.Substring(0, 4);

                    //  STEEK GEGEVENS IN STRUCT
                    newRecord.beginDag = beginDag;
                    newRecord.eindDag = eindDag;
                    newRecord.type = "1";
                    newRecord.id = "1";
                    newRecord.ucat = "0";

                    //  VOEG STRUCT TOE AAN ARRAYLIST
                    records.Add(newRecord);
                }

                //  RETURN ARRAYLIST
                return records;
            }
            finally
            {
                // SLUIT DE READER
                if (rdr != null)
                {
                    rdr.Close();
                }
                //  SLUIT SQL CONNECTIE
                closeConnection(conn);
            }
        }

        //  HAAL RECORD OP (met POST/GET VAR id) VOOR VULLEN FORM VANUIT FLASH
        public void getRecord()
        {
            //  OPEN SQL CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  DEFINIEER VARS
            String beginDag, eindDag, van, tot;

            try
            {
                //  VOER SQL COMMAND UIT
                SqlCommand cmd = new SqlCommand("select MIN(datum) 'beginDag', MAX(datum) 'eindDag', type, aanvraagId 'id', COUNT(status) 'status', van, tot,ucatId,extraAanvraag from aanvraag,aanvraagDetail WHERE aanvraag.id = aanvraagId AND status<>1 AND aanvraag.id='" + Request["id"] + "' GROUP BY aanvraagId,type,van,tot,ucatId,extraAanvraag", conn);
                rdr = cmd.ExecuteReader();
                int extraAanvr = 0;
                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    //  HERSCHIK DATUMS, UREN
                    beginDag = Convert.ToString(rdr["beginDag"]);
                    beginDag = beginDag.Substring(6, 2) + "/" + beginDag.Substring(4, 2) + "/" + beginDag.Substring(0, 4);

                    eindDag = Convert.ToString(rdr["eindDag"]);
                    eindDag = eindDag.Substring(6, 2) + "/" + eindDag.Substring(4, 2) + "/" + eindDag.Substring(0, 4);

                    van = Convert.ToString(rdr["van"]);
                    van = van.Substring(0, 2) + ":" + van.Substring(2, 2);

                    tot = Convert.ToString(rdr["tot"]);
                    tot = tot.Substring(0, 2) + ":" + tot.Substring(2, 2);

                    
                    if((Convert.ToInt32(rdr["extraAanvraag"])>0)||(beginDag!=eindDag)||(Convert.ToInt32(rdr["type"])==1))
                    {
                        extraAanvr = 1;
                    }
                    //  SCHRIJF GEGEVENS WEG VOOR OPHALEN MET AJAX
                    Response.Write(beginDag + ";" + eindDag + ";" + rdr["type"] + ";" + rdr["id"] + ";" + rdr["status"] + ";" + van + ";" + tot + ";" + rdr["ucatId"] + ";" + extraAanvr + ";");
                }
            }
            finally
            {
                // SLUIT READER
                if (rdr != null)
                {
                    rdr.Close();
                }
                //  SLUIT SQL CONNECTIE
                closeConnection(conn);
            }
        }
        
        //  HAAL ALLE RECORDS OP VOOR BEPAALD SALNUM (voor laden flashkalender, vanuit default.aspx)
        public ArrayList getRecord(string salnum)
        {
            //  OPEN SQL CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  DEFINIEER VARS
            String beginDag, eindDag, van, tot;
            ArrayList records = new ArrayList();
            record newRecord = new record();

            try
            {
                //  VOER SQL COMMANDO UIT
                SqlCommand cmd = new SqlCommand("select MIN(datum) 'beginDag', MAX(datum) 'eindDag', aanvraagDetail.type, aanvraagId 'id', status 'status', van, tot,ucatId,R,G,B from aanvraag,aanvraagDetail,ucat WHERE aanvraagDetail.datum<>'00000000' AND aanvraag.id = aanvraagId AND status <>1 AND werknemerSalnum='" + salnum + "' AND ucatId=ucat.id GROUP BY aanvraagId,R,G,B,aanvraagDetail.type,van,tot,ucatId,status", conn);
                rdr = cmd.ExecuteReader();

                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    //  HERSCHIK DATA EN UREN
                    beginDag = Convert.ToString(rdr["beginDag"]);
                    beginDag = beginDag.Substring(6, 2) + "/" + beginDag.Substring(4, 2) + "/" + beginDag.Substring(0, 4);

                    eindDag = Convert.ToString(rdr["eindDag"]);
                    eindDag = eindDag.Substring(6, 2) + "/" + eindDag.Substring(4, 2) + "/" + eindDag.Substring(0, 4);

                    van = Convert.ToString(rdr["van"]);
                    van = van.Substring(0, 2) + ":" + van.Substring(2, 2);

                    tot = Convert.ToString(rdr["tot"]);
                    tot = tot.Substring(0, 2) + ":" + tot.Substring(2, 2);

                    //  STEEK GEGEVENS IN STRUCT
                    newRecord.beginDag = beginDag;
                    newRecord.eindDag = eindDag;
                    newRecord.type = Convert.ToString(rdr["type"]);
                    newRecord.id = Convert.ToString(rdr["id"]);
                    newRecord.status = Convert.ToString(rdr["id"]);
                    newRecord.van = van;
                    newRecord.tot = tot;
                    newRecord.ucat = Convert.ToString(rdr["ucatId"]);
                    if (Convert.ToInt32(rdr["status"]) == 0)
                    {
                        newRecord.kleur = "000-176-240";
                    }
                    else if(Convert.ToInt32(rdr["status"]) == 1)
                    {
                        newRecord.kleur = "000-000-255";
                    }
                    else if (Convert.ToInt32(rdr["status"]) == 2)
                    {
                        newRecord.kleur = "255-000-000";
                    }
                    else if (Convert.ToInt32(rdr["status"]) == 3)
                    {
                        newRecord.kleur = "255-227-109";
                    }
                    else
                    {
                        newRecord.kleur = Convert.ToString(rdr["R"] + "-" + rdr["G"] + "-" + rdr["B"]);
                    }
                    //  VOEG STRUCT TOE AAN ARRAYLIST
                    records.Add(newRecord); 
                }

                //  RETURN ARRAYLIST
                return records;
            }
            finally
            {
                // SLUIT READER
                if (rdr != null)
                {
                    rdr.Close();
                }
                //  SLUIT CONNECTIE
                closeConnection(conn);
            }
        }

        //  RETURN ALLE UCATS
        public ArrayList getUcats()
        {
            //  OPEN DB CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  DEFINIEER VARS
            ArrayList ucat = new ArrayList();
            ucat newRecord = new ucat();

            try
            {
                //  VOER SQL CMD UIT
                SqlCommand cmd = new SqlCommand("select id,omschrijving,geldig,min,type,r,g,b FROM ucat", conn);
                rdr = cmd.ExecuteReader();

                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    //  VOEG TOE AAN STRUCT
                    newRecord.id = Convert.ToInt32(rdr["id"]);
                    newRecord.omschrijving = Convert.ToString(rdr["omschrijving"]);
                    newRecord.min = Convert.ToInt32(rdr["min"]);
                    newRecord.geldig = Convert.ToInt32(rdr["geldig"]);
                    newRecord.type = Convert.ToString(rdr["type"]);
                    newRecord.r = Convert.ToInt32(rdr["r"]);
                    newRecord.g = Convert.ToInt32(rdr["g"]);
                    newRecord.b = Convert.ToInt32(rdr["b"]);
                    //  STEEK STRUCT IN ARRAYLIST
                    ucat.Add(newRecord);
                }

                //RETURN ARRAYLIST
                return ucat;
            }
            finally
            {
                // SLUIT READER
                if (rdr != null)
                {
                    rdr.Close();
                }
                //  SLUIT SQL CONNECTIE
                closeConnection(conn);
            }
        }

        //  BIJ LADEN PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            //  HAAL RECORD OP VOOR LADEN FORM
            getRecord();
        }

        //  SLUIT CONNECTIES
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }

        //  CHECK LOGIN
        public int checkLogin(string login)
        {
            //  OPEN DB CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  VOER SQL CMD UIT
            SqlCommand cmd = new SqlCommand("select COUNT(salnum) 'aantal',salnum,type,naam FROM werknemer WHERE badgenr='"+login+"' GROUP BY salnum,type,naam", conn);

            rdr = cmd.ExecuteReader();
            rdr.Read();
            try
            {
                if (Convert.ToInt32(rdr["aantal"]) == 1)
                {
                    Session.Add("salnum", rdr["salnum"]);
                    Session.Add("type", rdr["type"]);
                    Session.Add("naam",rdr["naam"]);
                }
                return Convert.ToInt32(rdr["aantal"]);
            }
            catch(Exception ex)
	        {
                return 0;
	        }
        }
        public string teller(int jaar)
        {
            //  MAAK CONNECTIE MET DB
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;

            //  DEFINIEER VARS
            ArrayList tellers = new ArrayList();
            ArrayList ucats = new ArrayList();
            
            try
            {
                //  VOER SQL CMD UIT
                SqlCommand cmd = new SqlCommand("select saldo,ucatId from teller WHERE werknemerSalnum='" + Session["salnum"] + "' AND jaar='"+jaar+"'", conn);
                rdr = cmd.ExecuteReader();

                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    tellers.Add(rdr["saldo"]);
                    ucats.Add(rdr["ucatId"]);
                }
            }
            finally
            {
                closeConnection(conn);
            }
            int id;
            string returnw = "";
            id = ucats.IndexOf(47);
            returnw = Convert.ToString(id);
            if(id>=0)
                returnw = tellers[id].ToString();

            returnw += ";";

            id = ucats.IndexOf(21);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(26);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(43);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(49);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(58);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(60);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(62);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(34);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(34);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            id = ucats.IndexOf(100);
            if (id >= 0)
                returnw += tellers[id];

            returnw += ";";

            return returnw;
        }
    }
}
