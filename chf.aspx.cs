﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class chf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;
            SqlCommand cmd = new SqlCommand("UPDATE aanvraagDetail SET aanvraagDetail.status='"+Request["type"]+"', aanvraagDetail.export=1 WHERE aanvraagId = '"+Request["nmbr"]+"'", conn);
            cmd.ExecuteScalar();

            
            Response.Write("De aanvraag is succesvol aangepast");
            
        }
        //  SLUIT CONNECTIES
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
    }
}
