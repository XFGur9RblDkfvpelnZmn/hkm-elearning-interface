﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;

namespace WebApplication8
{
    public partial class opslaan : System.Web.UI.Page
    {
        //  BIJ LADEN PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-BE");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("nl-BE");

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            //  OPEN SQL CONNECTIE
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;
            int id;
            char[] delimiters = new char[] { '/', '-', '.', ':', '+', '=', '_', ' ', '*', '&' };
            SqlCommand scPopulatie = new SqlCommand("SELECT populatie 'pop' FROM werknemer WHERE salnum='" + Session["salnum"] + "'", conn);
            SqlDataReader rdr2 = scPopulatie.ExecuteReader();
            rdr2.Read();
            char pop = Convert.ToChar(rdr2["pop"]);
            rdr2.Close();
            if ((Convert.ToString(Request["ucat"]).Length > 0) && ((Convert.ToString(Request["type"]) != "0") || ((Convert.ToString(Request["van"]).Length > 4) && (Convert.ToString(Request["tot"]).Length > 4))))
            {


                string vanu = "";
                string totu = "";
                decimal van = 0, tot = 0;
                if (Convert.ToInt32(Request["type"]) == 0)
                {

                    try
                    {
                        vanu = Convert.ToString(Request["van"]).Substring(0, 2) + Convert.ToString(Request["van"]).Substring(3, 2);
                        totu = Convert.ToString(Request["tot"]).Substring(0, 2) + Convert.ToString(Request["tot"]).Substring(3, 2);
                        int hulp = Convert.ToInt32(Convert.ToString(vanu).Substring(2, 2));
                        hulp = hulp / 3 * 5;
                        van = Convert.ToDecimal(Convert.ToString(vanu).Substring(0, 2) + "." + hulp.ToString("00"));
                        hulp = Convert.ToInt32(Convert.ToString(totu).Substring(2, 2));
                        hulp = hulp / 3 * 5;
                        tot = Convert.ToDecimal(Convert.ToString(totu).Substring(0, 2) + "." + hulp.ToString("00"));
                        //Response.Write(Convert.ToString(Convert.ToString(vanu).Substring(0, 2) + "," + Convert.ToString(vanu).Substring(2, 2) + "m"));

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                    }
                }
                string[] dagen = Request["eindDag"].Split(';');
                if (dagen.Length < 2)
                {
                    //  MAAK DATETIME AAN VOOR BEGIN- EN EINDDAG
                    string[] split = Request["beginDag"].Split(delimiters);
                    if (split[2].Length < 3)
                    {
                        split[2] = "20" + split[2];
                    }
                    DateTime dag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));
                    split = Request["eindDag"].Split(delimiters);
                    DateTime eindDag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));
                    DateTime tel = dag;
                    decimal saldoNodig = 0m;
                    string error = "";
                    while (tel <= eindDag)
                    {
                        SqlCommand checkPlanning = new SqlCommand("SELECT minuten 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag = '" + tel.Year.ToString("0000") + tel.Month.ToString("00") + tel.Day.ToString("00") + "'", conn);
                        //Response.Write("SELECT minuten 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag = '" + tel.Year.ToString("0000") + tel.Month.ToString("00") + tel.Day.ToString("00") + "'");
                        //Response.Write("SELECT SUM(minuten) 'minuten' FROM planning,werknemer WHERE werknemerRecnum=recordnr AND salnum='" + Session["salnum"] + "' AND dag >= '" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "' AND dag <= '" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "'");
                        rdr = checkPlanning.ExecuteReader();
                        rdr.Read();
                        if (rdr.HasRows)
                        {
                            switch (Convert.ToInt32(Request["type"]))
                            {
                                case 1:
                                    try
                                    {
                                        decimal d = Convert.ToDecimal(rdr["minuten"]) / 60;
                                        if (d >= 6m)
                                        {
                                            d -= 0.5m;
                                        }
                                        saldoNodig += d;
                                        
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    break;
                                case 2:
                                case 3:

                                    try
                                    {
                                        
                                        decimal check = Convert.ToDecimal(rdr["minuten"]) / 60;
                                        if ((check) >= Convert.ToDecimal(8) || pop == 'G')
                                        {
                                            saldoNodig += Convert.ToDecimal(rdr["minuten"]) / 120;
                                            
                                        }
                                        else
                                        {
                                            Response.Write("Op deze dag(en) kan geen halve dag aangevraagd worden.");
                                            error = "fout";
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                    break;
                                case 0:
                                    try
                                    {
                                       
                                        if (((van * 60) == Convert.ToDecimal(rdr["van"])) && ((tot * 60) == (Convert.ToDecimal(rdr["van"]) + Convert.ToDecimal(rdr["minuten"]))))
                                        {
                                            saldoNodig = Convert.ToDecimal(rdr["minuten"]) / 60;
                                            if (saldoNodig >= 6m)
                                            {
                                                saldoNodig -= 0.5m;
                                            }
                                            
                                        }
                                        else if (((van * 60) == Convert.ToDecimal(rdr["van"])) || ((tot * 60) == (Convert.ToDecimal(rdr["van"]) + Convert.ToDecimal(rdr["minuten"])))||pop == 'G')
                                        {
                                            decimal d = tot - van;
                                            if (d >= 6m)
                                            {
                                                d -= 0.5m;
                                            }
                                            saldoNodig += d;
                                            
                                        }
                                        else
                                        {
                                            error = "fout";
                                            Response.Write("Een aanvraag met van- en totuur moet op het begin of einde van je planning liggen.");
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    break;
                            }
                        }
                        rdr.Close();
                        tel = tel.AddDays(1);
                    }


                    //Response.Write(Convert.ToString(minuten));
                    SqlCommand checkTeller = new SqlCommand("SELECT saldo FROM teller WHERE jaar = '"+eindDag.Year.ToString("0000")+"' AND werknemerSalnum = '" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                    rdr = checkTeller.ExecuteReader();
                    rdr.Read();
                    decimal saldo;
                    if (rdr.HasRows)
                    {
                         saldo = Convert.ToDecimal(rdr["saldo"]);
                    }
                    else
                    {
                        saldo = 0;
                    }
                    rdr.Close();
                    SqlCommand checkMinTeller = new SqlCommand("SELECT [min] FROM ucat where id='"+Request["ucat"].ToString().Trim()+"'", conn);
                    rdr = checkMinTeller.ExecuteReader();
                    rdr.Read();
                    decimal min;
                    if (rdr.HasRows)
                    {
                        min = Convert.ToDecimal(rdr["min"]);
                    }
                    else
                    {
                        min = 0;
                    }
                    rdr.Close();
                    
                    //Response.Write(Convert.ToString(rdr["saldo"]) +"/"+ Convert.ToString(totaal)+"/"+Convert.ToString(minuten)+"#"+Convert.ToString(checkPlanning));
                    if ((saldo >= saldoNodig-min)&&(error.Length<1))
                    {

                        SqlCommand saldoUpdate = new SqlCommand("UPDATE teller SET saldo=saldo-'" + saldoNodig.ToString("#0.00").Replace(',','.') + "' WHERE jaar = '"+eindDag.Year.ToString("0000")+"' AND werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                        saldoUpdate.ExecuteNonQuery();
                        //Response.Write("UPDATE teller SET saldo=saldo-'" + totaal + "' WHERE werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')");
                        //MAAK NIEUW RECORD AAN IN aanvraag
                        SqlCommand cmd = new SqlCommand("INSERT into aanvraag (werknemerSalnum, ucatId) values ('" + Session["salnum"] + "','" + Request["ucat"] + "')", conn);
                        try
                        {
                            cmd.ExecuteScalar();
                        }
                        finally
                        {
                            //  SELECTEER ID VAN aanvraag
                            SqlCommand getMax = new SqlCommand("SELECT MAX(id) 'id' FROM aanvraag WHERE werknemerSalnum='" + Session["salnum"] + "'", conn);
                            rdr = getMax.ExecuteReader();
                            rdr.Read();
                            id = Convert.ToInt32(rdr["id"]);
                            rdr.Close();
                            int geheel = Convert.ToInt32(Math.Floor(saldoNodig));
                            decimal deel = (saldoNodig - geheel)*100;
                            deel = deel / 10 * 6;

                            //MAAK PDF VOOR CODE 46 & 45
                            SqlCommand selectPers = new SqlCommand("SELECT naam FROM werknemer WHERE salnum='" + Session["salnum"] + "'", conn);
                            rdr = selectPers.ExecuteReader();
                            rdr.Read();
                            if (Convert.ToInt32(Request["ucat"]) == 45)
                            {
                                pdf45.createDoc(rdr["naam"].ToString(), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(Request["type"]), Request["vanu"], Request["totu"], geheel.ToString("00") + ":" + deel.ToString("00"));
                            }
                            else if (Convert.ToInt32(Request["ucat"]) == 46)
                            {
                                pdf.createDoc(rdr["naam"].ToString(), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(Request["type"]), Request["vanu"], Request["totu"], geheel.ToString("00") + ":" + deel.ToString("00"));
                            }
                            rdr.Close();

                            //  VOOR ELKE TUSSENLIGGENDE DAG
                            while (dag <= eindDag)
                            {
                                //  MAAK EEN DETAILRECORD AAN
                                SqlCommand insertDetail = new SqlCommand("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')", conn);
                                //Response.Write("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')");
                                insertDetail.ExecuteNonQuery();

                                //  GA NAAR VOLGENDE DAG
                                dag = dag.AddDays(1);
                            }



                            //  SLUIT CONNECTIE
                            closeConnection(conn);
                        }
                    }
                    else if (error.Length > 0)
                    {
                        rdr.Close();
                    }
                    else
                    {
                        rdr.Close();
                        Response.Write("Uw saldo is te laag voor deze aanvraag.");
                    }
                }
                else
                {
                    for (int i = 0; i < dagen.Length; i++)
                    {
                        //  MAAK DATETIME AAN VOOR BEGIN- EN EINDDAG
                        string[] split = Request["beginDag"].Split(delimiters);
                        if (split[2].Length < 3)
                        {
                            split[2] = "20" + split[2];
                        }
                        DateTime dag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));
                        split = dagen[i].Split(delimiters);
                        DateTime eindDag = new DateTime(Convert.ToInt32(split[2]), Convert.ToInt32(split[1]), Convert.ToInt32(split[0]));

                        if (i == 0)
                        {
                            SqlCommand checkPlanning = new SqlCommand("SELECT SUM(minuten) 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag >= '" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "' AND dag <= '" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "'", conn);
                            rdr = checkPlanning.ExecuteReader();
                        }
                        else
                        {
                            SqlCommand checkPlanning = new SqlCommand("SELECT SUM(minuten) 'minuten',van FROM planning WHERE werknemerSalnum='" + Session["salnum"] + "' AND dag = '" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "'", conn);
                            rdr = checkPlanning.ExecuteReader();
                        }
                        //Response.Write("SELECT SUM(minuten) 'minuten' FROM planning,werknemer WHERE werknemerRecnum=recordnr AND salnum='" + Session["salnum"] + "' AND dag > '" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "' AND dag < '" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "'");

                        rdr.Read();
                        decimal minuten;
                        try
                        {
                            minuten = Convert.ToDecimal(rdr["minuten"]);
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex);
                            minuten = Convert.ToDecimal(0);
                        }
                        rdr.Close();


                        SqlCommand checkTeller = new SqlCommand("SELECT saldo FROM teller WHERE jaar ='"+eindDag.Year.ToString("0000")+"' werknemerSalnum = '" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                        rdr = checkTeller.ExecuteReader();
                        rdr.Read();
                        decimal saldo = Convert.ToDecimal(rdr["saldo"]);
                        decimal perdag = 0;
                        switch (Convert.ToInt32(Request["type"]))
                        {
                            case 1:
                                perdag = 9;
                                break;
                            case 2:
                            case 3:
                                perdag = 4.50m;
                                break;
                            case 0:
                                perdag = tot - van;
                                break;

                        }

                        decimal totaal = 0;
                        DateTime reken = dag;
                        if (i == 0)
                        {

                            while (reken <= eindDag)
                            {
                                totaal += perdag;
                                reken = reken.AddDays(1);

                            }
                        }
                        else
                        {
                            totaal = perdag;
                        }
                        rdr.Close();
                        if (totaal > (minuten / 60))
                        {
                            totaal = minuten / 60;
                        }
                        reken = dag;
                        while (reken <= eindDag)
                        {
                            totaal -= 0.5m;
                            reken = reken.AddDays(1);

                        }

                        SqlCommand saldoRead = new SqlCommand("SELECT saldo FROM teller WHERE jaar = '"+eindDag.Year.ToString("0000")+"' werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                        rdr = saldoRead.ExecuteReader();
                        rdr.Read();
                        SqlCommand checkMinTeller = new SqlCommand("SELECT [min] FROM ucat where id='" + Request["ucat"].ToString().Trim() + "'", conn);
                        rdr = checkMinTeller.ExecuteReader();
                        rdr.Read();
                        decimal min;
                        if (rdr.HasRows)
                        {
                            min = Convert.ToDecimal(rdr["min"]);
                        }
                        else
                        {
                            min = 0;
                        }
                        rdr.Close();
                        //Response.Write(Convert.ToString(rdr["saldo"]) + "/" + Convert.ToString(totaal) + "/" + Convert.ToString(minuten)+"#");
                        if (Convert.ToDecimal(rdr["saldo"]) >= totaal)
                        {
                            rdr.Close();
                            SqlCommand saldoUpdate = new SqlCommand("UPDATE teller SET saldo=saldo-'" + totaal + "' WHERE jaar = '"+eindDag.Year.ToString("0000")+"' werknemerSalnum='" + Session["salnum"] + "' AND ucatId=(SELECT tellerUcat FROM ucat WHERE ucat.id='" + Request["ucat"] + "')", conn);
                            saldoUpdate.ExecuteNonQuery();
                            //  MAAK NIEUW RECORD AAN IN aanvraag
                            SqlCommand cmd = new SqlCommand("INSERT into aanvraag (werknemerSalnum, ucatId) values ('" + Session["salnum"] + "','" + Request["ucat"] + "')", conn);
                            try
                            {
                                cmd.ExecuteScalar();
                            }
                            finally
                            {
                                //  SELECTEER ID VAN aanvraag
                                SqlCommand getMax = new SqlCommand("SELECT MAX(id) 'id' FROM aanvraag WHERE werknemerSalnum='" + Session["salnum"] + "'", conn);
                                rdr = getMax.ExecuteReader();
                                rdr.Read();
                                id = Convert.ToInt32(rdr["id"]);
                                rdr.Close();

                                //MAAK PDF VOOR CODE 46 & 45
                                SqlCommand selectPers = new SqlCommand("SELECT naam FROM werknemer WHERE salnum='" + Session["salnum"] + "'", conn);
                                rdr = selectPers.ExecuteReader();
                                rdr.Read();
                                int geheel = Convert.ToInt32(Math.Floor(totaal));
                                decimal deel = (totaal - geheel) * 100;
                                deel = deel / 10 * 6;
                                if (Convert.ToInt32(Request["ucat"]) == 45)
                                {
                                    pdf45.createDoc(Convert.ToString(rdr["naam"]), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(Request["type"]), Request["vanu"], Request["totu"], geheel.ToString("00")+":"+deel.ToString("00"));
                                }
                                else if (Convert.ToInt32(Request["ucat"]) == 46)
                                {
                                    pdf.createDoc(Convert.ToString(rdr["naam"]), dag.Day.ToString("00") + "/" + dag.Month.ToString("00") + "/" + dag.Year.ToString("0000"), eindDag.Day.ToString("00") + "/" + eindDag.Month.ToString("00") + "/" + eindDag.Year.ToString("0000"), Convert.ToInt32(Request["type"]), Request["vanu"], Request["totu"], geheel.ToString("00") + ":" + deel.ToString("00"));
                                }
                                rdr.Close();

                                if (i == 0)
                                {
                                    //  VOOR ELKE TUSSENLIGGENDE DAG
                                    while (dag <= eindDag)
                                    {
                                        //  MAAK EEN DETAILRECORD AAN
                                        SqlCommand insertDetail = new SqlCommand("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')", conn);
                                        //Response.Write("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')");
                                        insertDetail.ExecuteNonQuery();

                                        //  GA NAAR VOLGENDE DAG
                                        dag = dag.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //  MAAK EEN DETAILRECORD AAN
                                    SqlCommand insertDetail = new SqlCommand("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + eindDag.Year.ToString("0000") + eindDag.Month.ToString("00") + eindDag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')", conn);
                                    //Response.Write("INSERT into aanvraagDetail (datum,van,tot,type,aanvraagId) values ('" + dag.Year.ToString("0000") + dag.Month.ToString("00") + dag.Day.ToString("00") + "','" + vanu + "','" + totu + "','" + Request["type"] + "','" + id + "')");
                                    insertDetail.ExecuteNonQuery();
                                }
                            }
                            

                        }
                        else
                        {
                            Response.Write("Uw saldo is te laag voor deze aanvraag.");
                            rdr.Close();
                        }
                    }
                    //  SLUIT CONNECTIE
                    closeConnection(conn);
                }

            }
            else
            {
                Response.Write("Gelieve alle velden in te vullen, uw aanvraag is niet verwerkt!");
            }
            
        }

        //  CONNECTIE SLUITEN
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                conn.Close();
            }
        }

        //  CONNECTIE OPENEN
        protected SqlConnection createConnection()
        {
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            return conn;
        }
    }
}
