﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;

namespace WebApplication8
{
    public partial class Annuleer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.SelectCommand = "SELECT MIN(aanvraagDetail.datum) AS 'beginSort', MAX(aanvraagDetail.datum) AS 'eindSort',aanvraag.id AS 'AANVRAAG', aanvraag.werknemerSalnum, aanvraag.ucatId, SUBSTRING(MIN(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 1, 4) AS Begindag, SUBSTRING(MAX(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 1, 4) AS Einddag, CASE WHEN status = 0 THEN 'IN AANVRAAG' WHEN status = 1 THEN 'ANNULATIE' WHEN status = 2 THEN 'AFKEURING' WHEN status= 3 THEN 'IN WACHT' WHEN status=4 THEN 'GOEDGEKEURD' END AS statusText, status, CASE WHEN aanvraagDetail.type = 1 THEN 'HELE DAG' WHEN aanvraagDetail.type = 2 THEN 'VOORMIDDAG' WHEN aanvraagDetail.type = 3 THEN 'NAMIDDAG' WHEN aanvraagDetail.type = 0 THEN 'VAN/TOT' END AS type, SUBSTRING(aanvraagDetail.van, 1, 2) + ':' + SUBSTRING(aanvraagDetail.van, 3, 2) AS 'van', SUBSTRING(aanvraagDetail.tot, 1, 2) + ':' + SUBSTRING(aanvraagDetail.tot, 3, 2) AS 'tot' FROM aanvraag INNER JOIN aanvraagDetail ON aanvraag.id = aanvraagDetail.aanvraagId INNER JOIN werknemer ON aanvraag.werknemerSalnum = werknemer.salnum WHERE aanvraag.id='"+Request["id"]+"' GROUP BY aanvraagDetail.aanvraagId, aanvraag.werknemerSalnum, aanvraag.ucatId, aanvraagDetail.status, aanvraagDetail.type, aanvraagDetail.van, aanvraagDetail.tot, aanvraag.id";
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
    }
        
}

