﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace QuizSalesFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string sPath = args[0];
                try
                {
                    StreamReader sr = new StreamReader(args[0]);
                    string sContent = sr.ReadToEnd();
                    sr.Close();
                    string sContentNew = string.Empty;
                    string sDate = string.Empty;
                    string sCode = string.Empty;
                    foreach (string sLine in Regex.Split(sContent, Environment.NewLine))
                    {
                        
                        string sLineNew = sLine;

                        if (sLine.Length > 0)
                        {
                            string sType = sLine.Substring(0, 1);
                            if (sType == "0")
                            {
                                try
                                {
                                    sDate = sLine.Split(',')[2];
                                    sCode = sLine.Split(',')[4];
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Fout in 0-regel");
                                    sDate = string.Empty;
                                    sCode = string.Empty;
                                }
                            }
                            else if (sType == "1")
                            {
                                sLineNew = sLine + ',' + sDate + ',' + sCode;
                            }
                            sContentNew += sLineNew + Environment.NewLine;

                        }
                    }
                    StreamWriter sw = new StreamWriter(sPath);
                    sw.Write(sContentNew);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
            else
            {
                Console.WriteLine("Parameter 1 (path\\file) cannot be null");
            }

        }
    }
}
