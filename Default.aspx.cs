﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApplication8
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  NEEM SALNUM UIT SESSIE
            string salnum = Session["salnum"].ToString();
            
            //  DEFINIEER AANVRAAGBESTAND
            getAanvraagRecord aanvraag = new getAanvraagRecord();

            //  HAAL RECORDS OP EN STEEK DEZE IN ARRAYLIST
            ArrayList line = new ArrayList();
            line = aanvraag.getRecord(salnum);

            //  GEEF ALLE RECORDS WEER
            for(int i=0;i<line.Count;i++)
            {
                Response.Write(((getAanvraagRecord.record)line[i]).beginDag);
                Response.Write(";");
                Response.Write(((getAanvraagRecord.record)line[i]).eindDag);
                Response.Write(";");
                Response.Write(((getAanvraagRecord.record)line[i]).kleur);
                Response.Write(";");
                Response.Write(((getAanvraagRecord.record)line[i]).id);
                Response.Write("|");
            }

            
        }

    }
}
