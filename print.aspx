﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print.aspx.cs" Inherits="WebApplication8.print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
</head>
<body onload="javascript:print();">
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Begindag" HeaderText="Begindag" ReadOnly="True" 
                    SortExpression="Begindag" />
                <asp:BoundField DataField="Einddag" HeaderText="Einddag" ReadOnly="True" 
                    SortExpression="Einddag" />
                <asp:BoundField DataField="van" HeaderText="van" SortExpression="van" />
                <asp:BoundField DataField="tot" HeaderText="tot" SortExpression="tot" />
                <asp:BoundField DataField="status" HeaderText="status" 
                    SortExpression="status" />
                <asp:BoundField DataField="type" HeaderText="type" SortExpression="type" />
                <asp:BoundField DataField="ucat" HeaderText="Categorie" SortExpression="ucat" />
            </Columns>
        </asp:GridView>

    </div>
    </form>
</body>
</html>
