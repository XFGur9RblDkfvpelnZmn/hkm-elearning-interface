﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class Export : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Parms.leesParms();
            SqlConnection conn = createConnection();

            // create a writer and open the file
            TextWriter tw = new StreamWriter(Parms.expHoppla);

            //  VOER SQL CMD UIT
            SqlCommand cmd = new SqlCommand("SELECT planning.minuten, planning.van AS vanaf, aanvraag.werknemerSalnum AS 'salnum', aanvraag.ucatId, orig.datum, orig.van, orig.tot, orig.type, orig.status,orig.extraAanvraag, (SELECT van FROM aanvraagDetail WHERE aanvraagDetail.aanvraagId=orig.extraAanvraag) AS 'vanExtra', (SELECT tot FROM aanvraagDetail WHERE aanvraagDetail.aanvraagId=orig.extraAanvraag) AS 'totExtra', (SELECT type FROM aanvraagDetail WHERE aanvraagDetail.aanvraagId=orig.extraAanvraag) AS 'typeExtra', (SELECT ucatId FROM aanvraag WHERE aanvraag.id=orig.extraAanvraag) AS 'ucatExtra' FROM aanvraagDetail AS orig INNER JOIN aanvraag ON orig.aanvraagId = aanvraag.id INNER JOIN planning ON aanvraag.werknemerSalnum = planning.werknemerSalnum AND orig.datum = planning.dag WHERE (orig.export<2) and orig.status > 0", conn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //  VOOR ELK RECORD
            while (rdr.Read())
            {
                
                if (Convert.ToInt32(rdr["type"]) == 1)
                {
                    decimal vanaf = Convert.ToDecimal(rdr["vanaf"]) / 60;
                    decimal vanafMin = (vanaf-Math.Floor(vanaf)) / 5 * 3*100;
                    decimal tot = (Convert.ToDecimal(rdr["vanaf"]) + Convert.ToDecimal(rdr["minuten"])) / 60;
                    decimal totMin = (tot-Math.Floor(tot)) / 5 * 3*100;
                    string ucatId = Convert.ToString(rdr["ucatId"]);
                    if (ucatId.Trim() == "100")
                    {
                        ucatId = "05";
                    }
                    if (rdr["status"].ToString() == "3")
                    {
                        ucatId = Parms.ucatVoorlopig;
                    }
                    else if (rdr["status"].ToString() == "2")
                    {
                        ucatId = "";
                    }
                    if ((vanaf == 0m) && (vanafMin == 0m) && (tot < 1m) && (totMin < 1m))
                    {
                        tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";0000;0000;;;;");
                    }
                    else
                    {
                        tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + totMin.ToString("00") + ";;;;");
                    }
                }
                else if (Convert.ToInt32(rdr["type"]) == 2)
                {
                    decimal tot = (Convert.ToDecimal(rdr["minuten"])/120 + Convert.ToDecimal(rdr["vanaf"])/60);
                    decimal totMin = (tot - Math.Floor(tot)) / 5 * 3 * 100;
                    decimal vanaf = Convert.ToDecimal(rdr["vanaf"]) / 60;
                    decimal vanafMin = (vanaf - Math.Floor(vanaf)) / 5 * 3 * 100;
                    decimal dagdeel2 = tot + Convert.ToDecimal(rdr["minuten"]) / 120;
                    decimal dagdeel2Min = (dagdeel2 - Math.Floor(dagdeel2)) / 5 * 3 * 100;
                    string ucatId = Convert.ToString(rdr["ucatId"]);
                    if (ucatId.Trim() == "100")
                    {
                        ucatId = "05";
                    }
                    if (rdr["status"].ToString() == "3")
                    {
                        ucatId = Parms.ucatVoorlopig;
                    }
                    else if (rdr["status"].ToString() == "2")
                    {
                        ucatId = "";
                    }
                    if (Convert.ToInt32(rdr["extraAanvraag"]) > 0)
                    {
                        if (Convert.ToInt32(rdr["typeExtra"]) == 0)
                        {
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Convert.ToString(rdr["ucatExtra"]) + ";" + Convert.ToString(rdr["vanExtra"]) + ";" + Convert.ToString(rdr["totExtra"]) + ";");
                        }
                        else
                        {
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Convert.ToString(rdr["ucatExtra"]) + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Math.Floor(dagdeel2).ToString("00") + Math.Floor(dagdeel2Min).ToString("00") + ";");
                        }
                    }
                    else
                    {
                        tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";;" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Math.Floor(dagdeel2).ToString("00") + Math.Floor(dagdeel2Min).ToString("00") + ";");
                    }
                }
                else if (Convert.ToInt32(rdr["type"]) == 3)
                {
                    decimal tot = (Convert.ToDecimal(rdr["minuten"]) / 120 + Convert.ToDecimal(rdr["vanaf"]) / 60);
                    decimal totMin = (tot - Math.Floor(tot)) / 5 * 3 * 100;
                    decimal vanaf = Convert.ToDecimal(rdr["vanaf"]) / 60;
                    decimal vanafMin = (vanaf - Math.Floor(vanaf)) / 5 * 3 * 100;
                    decimal dagdeel2 = tot + Convert.ToDecimal(rdr["minuten"]) / 120;
                    decimal dagdeel2Min = (dagdeel2 - Math.Floor(dagdeel2)) / 5 * 3 * 100;
                    string ucatId = Convert.ToString(rdr["ucatId"]);
                    if (ucatId.Trim() == "100")
                    {
                        ucatId = "05";
                    }
                    if (rdr["status"].ToString() == "3")
                    {
                        ucatId = Parms.ucatVoorlopig;
                    }
                    else if (rdr["status"].ToString() == "2")
                    {
                        ucatId = "";
                    }
                    if (Convert.ToInt32(rdr["extraAanvraag"]) > 0)
                    {
                        if (Convert.ToInt32(rdr["typeExtra"]) == 0)
                        {
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Math.Floor(dagdeel2).ToString("00") + Math.Floor(dagdeel2Min).ToString("00") + ";" + Convert.ToString(rdr["ucatExtra"]) + ";" + Convert.ToString(rdr["vanExtra"]) + ";" + Convert.ToString(rdr["totExtra"]) + ";");
                        }
                        else
                        {
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Math.Floor(dagdeel2).ToString("00") + Math.Floor(dagdeel2Min).ToString("00") + ";" + Convert.ToString(rdr["ucatExtra"]) + ";" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + totMin.ToString("00") + ";");
                        }
                    }
                    else
                    {
                        tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";" + Math.Floor(dagdeel2).ToString("00") + Math.Floor(dagdeel2Min).ToString("00") + ";;" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Math.Floor(tot).ToString("00") + Math.Floor(totMin).ToString("00") + ";");
                    }
                }
                else
                {
                    decimal vanaf = Convert.ToDecimal(rdr["vanaf"]) / 60;
                    decimal vanafMin = (vanaf - Math.Floor(vanaf)) / 5 * 3 * 100;
                    string ucatId = Convert.ToString(rdr["ucatId"]);
                    if (ucatId.Trim() == "100")
                    {
                        ucatId = "05";
                    }
                    if (rdr["status"].ToString() == "3")
                    {
                        ucatId = Parms.ucatVoorlopig;
                    }
                    else if (rdr["status"].ToString() == "2")
                    {
                        ucatId = "";
                    }
                    if (Convert.ToInt32(rdr["extraAanvraag"]) > 0)
                    {
                        tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + Convert.ToString(rdr["ucatId"]) + ";" + Convert.ToString(rdr["van"]) + ";" + Convert.ToString(rdr["tot"]) + ";" + rdr["ucatExtra"] + ";" + Convert.ToString(rdr["vanExtra"]) + ";" + Convert.ToString(rdr["totExtra"]) + ";");
                    }
                    else
                    {
                        if (Convert.ToString(rdr["van"]) == vanaf.ToString("00") + vanafMin.ToString("00"))
                        {
                            Response.Write("1");
                            decimal eind = (Convert.ToDecimal(rdr["vanaf"]) + Convert.ToDecimal(rdr["minuten"])) / 60;
                            decimal eindMin = (eind - Math.Floor(eind)) / 5 * 3 * 100;
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";" + ucatId + ";" + Convert.ToString(rdr["van"]) + ";" + Convert.ToString(rdr["tot"]) + ";;" + Convert.ToString(rdr["tot"]) + ";" + Math.Floor(eind).ToString("00") + Math.Floor(eindMin).ToString("00") + ";");
                        }
                        else
                        {
                            Response.Write("2");
                            decimal eind = (Convert.ToDecimal(rdr["vanaf"]) + Convert.ToDecimal(rdr["minuten"])) / 60;
                            decimal eindMin = (eind - Math.Floor(eind)) / 5 * 3 * 100;
                            vanaf = Convert.ToDecimal(rdr["vanaf"]) / 60;
                            vanafMin = (vanaf - Math.Floor(vanaf)) / 5 * 3 * 100;
                            tw.WriteLine(Convert.ToString(rdr["salnum"]) + ";" + Convert.ToString(rdr["datum"]) + ";;" + Math.Floor(vanaf).ToString("00") + vanafMin.ToString("00") + ";" + Convert.ToString(rdr["van"]) + ";" + ucatId + ";" + Convert.ToString(rdr["van"]) + ";" + Math.Floor(eind).ToString("00") + Math.Floor(eindMin).ToString("00") + ";");
                        }
                    }
                }
                
               
            }
            // write a line of text to the file


            // close the stream
            tw.Close();
            rdr.Close();
            SqlCommand cmd2 = new SqlCommand("UPDATE aanvraagDetail SET export = '2' WHERE     (aanvraagDetail.status >0) AND (aanvraagDetail.export<2)", conn);
            rdr = cmd2.ExecuteReader();
            closeConnection(conn);
        }
        //  SLUIT CONNECTIES
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
    }
}
