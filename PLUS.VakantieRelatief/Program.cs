﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Globalization;
using System.Threading;

namespace PLUS.VakantieRelatief
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("nl-NL");

                DateTime dtStart = DateTime.ParseExact(args[0], "yyMMdd", CultureInfo.InvariantCulture);
                DateTime dtEind = DateTime.ParseExact(args[1], "yyMMdd", CultureInfo.InvariantCulture);
                NUM num = NUM.GetByValue(args[2]);
                SEQ seq = SEQ.GetByValue(args[3]);
                int iLijst = Convert.ToInt32(args[4]);
                MODE mode = MODE.GetByValue(args[5]);
                FileInfo fi = new FileInfo(args[6]);
                string sFileName = Parameter.SSbRoot + "log\\file_" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
                FileInfo fiCopy = fi.CopyTo(sFileName);
                Proces.LfiWrkFiles.Add(fiCopy);
                COMPRESSED compressed = COMPRESSED.GetByValue(args[7]);
                compressed = COMPRESSED.NO;
                TIMEFRAME timeframe = TIMEFRAME.GetByValue(args[8]);
                int iSql = Convert.ToInt32(args[9]);

                List<List<string>> lls = Proces.LlsJrovz5pr(dtStart, dtEind, num, seq, iLijst, mode, fiCopy, COMPRESSED.YES, timeframe, iSql, TIMEFORMAT.HOURS_DECIMAL);
                List<string> lsWplekkod = new List<string>();
                List<decimal> ldFooter = new List<decimal>();
                int iGeldigVan = -1;
                int iGeldigTot = -1;
                int iVakBeg = -1;
                int iVakOpn = -1;
                int iWplekkod = -1;
                int i = 0;
                int iVakRel = -1;
                int iReserveer = -1;
                int iVorigJaar = -1;
                int iTvtSaldo = -1;
                foreach (string s in lls[0])
                {
                    if (s == "geldig van")
                    {
                        iGeldigVan = i;
                    }
                    else if (s == "geldig t/m")
                    {
                        iGeldigTot = i;
                    }
                    else if (s == "Huidig jr")
                    {
                        iVakBeg = i;
                    }
                    else if (s == "Opname jr")
                    {
                        iVakOpn = i;
                    }
                    else if (s == "Kostenplaats")
                    {
                        iWplekkod = i;
                    }
                    else if (s == "Reserveer")
                    {
                        iReserveer = i;
                    }
                    else if (s == "Vorig jr")
                    {
                        iVorigJaar = i;
                    }
                    else if (s == "Tvt saldo")
                    {
                        iTvtSaldo = i;
                    }
                    i++;
                }
                if (iGeldigTot == -1 || iGeldigVan == -1 || iVakBeg == -1 || iVakOpn == -1 || iWplekkod == -1 || iReserveer == -1 || iVorigJaar == -1 || iTvtSaldo == -1)
                {
                    Log.Verwerking("niet alle velden gevonden (iGeldigTot == " + iGeldigTot + " || iGeldigVan == " + iGeldigVan + " || Huidig jr == " + iVakBeg + " || Opname == " + iVakOpn + " || Kostenplaats == " + iWplekkod + " || Reserveer == " + iReserveer + " || iVorigJaar == "+iVorigJaar+" || iTvtSaldo == "+iTvtSaldo+")");
                }
                lls[0].Add("Vak Relatief");
                iVakRel = lls[0].Count-1;
                lls[0].Add("Vak Relatief - Opname");
                for (i = 1; i < lls.Count; i++)
                {

                    try
                    {
                        if (lls[i].Count >= iVakBeg && lls[i][iVakBeg] != string.Empty)
                        {
                            //indien wplekkod nog niet bestaat, voeg deze toe (cumulatie)
                            if (lsWplekkod.FindIndex(s => s == lls[i][iWplekkod].Trim()) < 0)
                            {
                                lsWplekkod.Add(lls[i][iWplekkod].Trim());
                            }
                            double dDeler = 0;
                            double dNoemer = 0;
                            double dFactor = 0;
                            //Situatie 1 : heel jaar in dienst
                            if (Convert.ToInt32(lls[i][iGeldigVan]) <= DateTime.Now.Year * 10000 + 0101 && Convert.ToInt32(lls[i][iGeldigTot]) >= DateTime.Now.Year * 10000 + 1231)
                            {
                                dDeler = (dtEind - (new DateTime(DateTime.Now.Year, 1, 1))).TotalDays;
                                dNoemer = (new DateTime(DateTime.Now.Year, 12, 31)).DayOfYear;
                                dFactor = dDeler / dNoemer;
                            }
                            //situatie 2 : uit dienst
                            else if (Convert.ToInt32(lls[i][iGeldigVan]) <= DateTime.Now.Year * 10000 + 0101 && Convert.ToInt32(lls[i][iGeldigTot]) < DateTime.Now.Year * 10000 + 1231)
                            {
                                if (Convert.ToInt32(dtEind.ToString("yyyyMMdd")) > Convert.ToInt32(lls[i][iGeldigTot]))
                                {
                                    dDeler = (DateTime.ParseExact(lls[i][iGeldigTot], "yyyyMMdd", CultureInfo.InvariantCulture) - (new DateTime(DateTime.Now.Year, 1, 1))).TotalDays;
                                }
                                else
                                {
                                    dDeler = (dtEind - (new DateTime(DateTime.Now.Year, 1, 1))).TotalDays;
                                }

                                dNoemer = (DateTime.ParseExact(lls[i][iGeldigTot], "yyyyMMdd", CultureInfo.InvariantCulture) - (new DateTime(DateTime.Now.Year, 1, 1))).TotalDays;
                                dFactor = dDeler / dNoemer;
                            }
                            //situatie 3 : in dienst
                            else if (Convert.ToInt32(lls[i][iGeldigVan]) > DateTime.Now.Year * 10000 + 0101 && Convert.ToInt32(lls[i][iGeldigTot]) >= DateTime.Now.Year * 10000 + 1231)
                            {
                                if (Convert.ToInt32(dtEind.ToString("yyyyMMdd")) >= Convert.ToInt32(lls[i][iGeldigVan]))
                                {
                                    dDeler = (dtEind - (DateTime.ParseExact(lls[i][iGeldigVan], "yyyyMMdd", CultureInfo.InvariantCulture))).TotalDays;
                                    dNoemer = ((new DateTime(DateTime.Now.Year,12,31)) - (DateTime.ParseExact(lls[i][iGeldigVan], "yyyyMMdd", CultureInfo.InvariantCulture))).TotalDays;
                                    dFactor = dDeler / dNoemer;
                                }
                                else
                                {
                                    dFactor = 0;
                                }

                                
                            }
                            //situatie 4 : in/uit dienst
                            else if (Convert.ToInt32(lls[i][iGeldigVan]) > DateTime.Now.Year * 10000 + 0101 && Convert.ToInt32(lls[i][iGeldigTot]) < DateTime.Now.Year * 10000 + 1231)
                            {
                                if (Convert.ToInt32(dtEind.ToString("yyyyMMdd")) >= Convert.ToInt32(lls[i][iGeldigVan]))
                                {
                                    if (Convert.ToInt32(dtEind.ToString("yyyyMMdd")) > Convert.ToInt32(lls[i][iGeldigTot]))
                                    {
                                        dDeler = (DateTime.ParseExact(lls[i][iGeldigTot], "yyyyMMdd", CultureInfo.InvariantCulture) - (DateTime.ParseExact(lls[i][iGeldigVan], "yyyyMMdd", CultureInfo.InvariantCulture))).TotalDays;
                                    }
                                    else
                                    {
                                        dDeler = (dtEind - (DateTime.ParseExact(lls[i][iGeldigVan], "yyyyMMdd", CultureInfo.InvariantCulture))).TotalDays;
                                    }
                                    dNoemer = ((DateTime.ParseExact(lls[i][iGeldigTot], "yyyyMMdd", CultureInfo.InvariantCulture)) - (DateTime.ParseExact(lls[i][iGeldigVan], "yyyyMMdd", CultureInfo.InvariantCulture))).TotalDays;
                                    dFactor = dDeler / dNoemer;
                                }
                                else
                                {
                                    dFactor = 0;
                                }
                            }
                            double dVakRelatief = Convert.ToDouble(lls[i][iVakBeg]) * dFactor;
                            double dVakRelatiefOpname = 0;
                            if (lls[i][iVakOpn].Trim() != string.Empty)
                            {
                                dVakRelatiefOpname = dVakRelatief - Convert.ToDouble(lls[i][iVakOpn]);
                            }
                            else
                            {
                                dVakRelatiefOpname = dVakRelatief;
                            }
                            lls[i].Add(dVakRelatief.ToString("0.00").Replace(".", ","));
                            lls[i].Add(dVakRelatiefOpname.ToString("0.00").Replace(".", ","));
                            /*
                            int iTot = Convert.ToInt32(dtEind.ToString("yyyyMMdd"));
                            int iVan = DateTime.Now.Year * 10000 + 0101;
                            if (Convert.ToInt32(lls[i][iGeldigTot]) < iTot)
                            {
                                iTot = Convert.ToInt32(lls[i][iGeldigTot]);
                            }
                            if (Convert.ToInt32(lls[i][iGeldigVan]) > iVan)
                            {
                                iVan = Convert.ToInt32(lls[i][iGeldigVan]);
                            }
                            decimal dDeler = Convert.ToDecimal((DateTime.ParseExact(iTot.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture) - DateTime.ParseExact(iVan.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture)).TotalDays) + 1;
                            decimal dNoemer = (new DateTime(DateTime.Now.Year, 12, 31)).DayOfYear;
                            decimal dFactor = dDeler / dNoemer;
                            decimal dVakRelatief = Convert.ToDecimal(lls[i][iVakBeg]) * dFactor;
                            lls[i].Add(dVakRelatief.ToString("0.00").Replace(".",","));
                              */
                        }
                        else
                        {
                            lls[i].Add("");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Verwerking("Foute tekenreeks");
                        foreach (string s in lls[i])
                        {
                            Log.Verwerking(s);
                        }
                        Log.Exception(ex);
                    }
                }
                if (compressed == COMPRESSED.YES)
                {
                    Proces.FiListToGrid("Vakantie Relatief", lls);
                }
                else
                {
                    List<List<string>> llsCompressed = new List<List<string>>();
                    llsCompressed.Add(new List<string>());
                    i = 0;
                    foreach (string s in lls[0])
                    {
                        if (i>= iWplekkod && i < iVakRel)
                        {
                            llsCompressed[0].Add(s);
                        }
                        i++;
                    }

                    foreach(string s in lsWplekkod)
                    {
                        List<string> lsWplekkodLijn = new List<string>();
                        for (i = iWplekkod; i < iVakRel+1; i++)
                        {
                            ldFooter.Add(0m);
                        }
                        for(i=iWplekkod;i<iVakRel;i++)
                        {
                            if(i!=iWplekkod && i != iVakBeg)
                            {
                                decimal total = 0;
                                string sAlternatief = string.Empty;
                                for (int a = 1; a < lls.Count; a++)
                                {
                                    
                                    if (lls[a].Count >= iVakBeg && lls[a][iVakBeg] != string.Empty)
                                    {
                                        if (lls[a][iWplekkod] == s)
                                        {
                                            if (i != iGeldigVan && i != iGeldigTot)
                                            {
                                                try
                                                {

                                                    decimal d = Convert.ToDecimal(lls[a][i]);
                                                    total += d;
                                                    ldFooter[i-iWplekkod+1] += d;
                                                }
                                                catch (Exception e)
                                                {
                                                    sAlternatief = lls[a][i];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (total > 0 || total < 0)
                                {
                                    lsWplekkodLijn.Add(total.ToString());
                                }
                                else
                                {
                                    lsWplekkodLijn.Add(sAlternatief);
                                }
                            }
                            else if (i == iVakBeg)
                            {
                                decimal total = 0;
                                string sAlternatief = string.Empty;
                                for (int a = 1; a < lls.Count; a++)
                                {

                                    if (lls[a].Count >= iVakBeg && lls[a][iVakBeg] != string.Empty)
                                    {
                                        if (lls[a][iWplekkod] == s)
                                        {
                                            if (i != iGeldigVan && i != iGeldigTot)
                                            {
                                                try
                                                {

                                                    decimal d = Convert.ToDecimal(lls[a][iVakRel]);
                                                    total += d;
                                                    ldFooter[i - iWplekkod+1] += d;
                                                }
                                                catch (Exception e)
                                                {
                                                    sAlternatief = lls[a][iVakRel];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (total > 0 || total < 0)
                                {
                                    lsWplekkodLijn.Add(total.ToString());
                                }
                                else
                                {
                                    lsWplekkodLijn.Add(sAlternatief);
                                }
                            }
                            else
                            {
                                lsWplekkodLijn.Add(s);
                            }
                        }
                        llsCompressed.Add(lsWplekkodLijn);
                    }
                    /*for (i = 1; i < lls.Count; i++)
                    {
                        for (int a = 0; a < lls[i].Count; a++)
                        {
                            if (a != iGeldigVan && a != iGeldigTot && a != iWplekkod)
                            {
                               try
                               {

                                   decimal d = Convert.ToDecimal(lls[i][a]);
                                   int iIndex = lsWplekkod.IndexOf(lls[i][iWplekkod]);
                                   llsCompressed[iIndex + 1][a] = (Convert.ToDecimal(llsCompressed[iIndex+1][a]) + d).ToString();
                               }
                               catch(Exception e)

                               {}
                            }
                        }
                    }*/
                    List<bool> lbool = new List<bool>();
                    for (i = 0; i < llsCompressed[0].Count; i++)
                    {
                        bool ok = true;
                        try
                        {
                            string sVgl = llsCompressed[1][i];
                            
                            for (int a = 2; a < llsCompressed.Count; a++)
                            {
                                if (lls[a].Count >= iVakBeg && lls[a][iVakBeg] != string.Empty)
                                {
                                    if (llsCompressed[a][i] != sVgl)
                                    {
                                        ok = false;
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                        lbool.Add(ok);

                    }
                    ldFooter[iReserveer - iWplekkod+1] = 0m;
                    foreach (List<string> ls in llsCompressed)
                    {
                        try
                        {
                            if (ls[iVorigJaar - iWplekkod].Length == 0)
                            {
                                ls[iVorigJaar - iWplekkod] = "0";
                            }
                            if (ls[iVakBeg - iWplekkod].Length == 0)
                            {
                                ls[iVakBeg - iWplekkod] = "0";
                            }
                            if (ls[iVakOpn - iWplekkod].Length == 0)
                            {
                                ls[iVakOpn - iWplekkod] = "0";
                            }
                            if (ls[iTvtSaldo - iWplekkod].Length == 0)
                            {
                                ls[iTvtSaldo - iWplekkod] = "0";
                            }
                            decimal d = (Convert.ToDecimal(ls[iVorigJaar - iWplekkod]) + Convert.ToDecimal(ls[iVakBeg - iWplekkod]) - Convert.ToDecimal(ls[iVakOpn - iWplekkod]) + Convert.ToDecimal(ls[iTvtSaldo - iWplekkod]));
                            ls[iReserveer - iWplekkod] = d.ToString();
                            ldFooter[iReserveer - iWplekkod+1] += d;
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    Proces.FiListToGridWithFooter("Vakantierectht reserveren t/m "+dtEind.ToString("dd-MM-yyyy"), llsCompressed,ldFooter);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                Proces.VerwijderWrkFiles();
            }
        }
    }
}
