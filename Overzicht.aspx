﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Overzicht.aspx.cs" Inherits="WebApplication8.Overzicht" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>html{font-family:verdana;font-size:11px;}
        @media print {
          #navi{display:none;}
          
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 200px;float:left" id="navi">
    Van:<br />
    <asp:Calendar ID="cdStartDatum" runat="server" Font-Names="Verdana" 
            Font-Size="11px">
        <DayHeaderStyle BackColor="#EFF3FC" />
        <DayStyle BackColor="#EFF3FC" />
        <NextPrevStyle BackColor="#507DD2" Font-Bold="True" ForeColor="White" />
        <TitleStyle BackColor="#507DD2" Font-Bold="True" ForeColor="White" />
        </asp:Calendar><br />
    Tot:<br />
    <asp:Calendar ID="cdEindDatum" runat="server" Font-Names="Verdana" 
            Font-Size="11px">
        <DayHeaderStyle BackColor="#EFF3FC" />
        <DayStyle BackColor="#EFF3FC" />
        <NextPrevStyle BackColor="#507DD2" Font-Bold="True" ForeColor="White" />
        <TitleStyle BackColor="#507DD2" Font-Bold="True" ForeColor="White" />
        </asp:Calendar>
    
    <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Vernieuwen" />
    <asp:Button Text="Afdrukken" runat="server" ID="btnPrint" OnClientClick="javascript:window.print();"/>
    </div>

    <center>
    <div id="divNaam" runat="server" style="width:100%; text-align:left"></div>
    <div id="divResult" runat = "server" style="width:100%; text-align:center">
    
    </div></center>
    </form>
</body>
</html>
