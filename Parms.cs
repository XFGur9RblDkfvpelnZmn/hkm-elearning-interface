﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Configuration;

namespace WebApplication8
{
    public class Parms
    {
        public static string expHoppla = ConfigurationSettings.AppSettings["expHoppla"];
        public static string expFolder = ConfigurationSettings.AppSettings["expFolder"];
        public static string hhoplaRoot = ConfigurationSettings.AppSettings["hhoplaRoot"];
        public static string ucatVoorlopig = ConfigurationSettings.AppSettings["ucatVoorlopig"];


        public static void leesParms()
        {
            WebClient client = new WebClient();
            Stream data = client.OpenRead("http://127.0.0.1/Parms.hhopla");
            StreamReader tr = new StreamReader(data);
            string line = null;
            for (int i = 0; i < 3; i++)
            {
                line = tr.ReadLine();
                switch (i)
                {
                    case 0:
                        expHoppla = line;
                        break;
                    case 1:
                        expFolder = line;
                        break;
                    case 2:
                        hhoplaRoot = line;
                        break;
                }
            }
        }
    }
}