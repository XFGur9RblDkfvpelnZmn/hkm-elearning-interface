﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication8.WebForm1" %>

<%@ Register assembly="FlashControl" namespace="Bewise.Web.UI.WebControls" tagprefix="Bewise" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Kalender van <% Response.Write(Session["salnum"]); Response.Write(Session["naam"]); %></title>
    <script language="javascript" type="text/javascript">

        //  selecteer flashmovie 
        function getFlashMovie(movieName) {
            var isIE = navigator.appName.indexOf("Microsoft") != -1;
            return (isIE) ? document.getElementById(movieName) : document[movieName];
        }
        function formSend() {
            var text = document.getElementById("txtVan").value;
            arr_text = text.split("/");

            var text2 = document.getElementById("txtTot").value;
            arr_text2 = text2.split("/");
            getFlashMovie("FlashControl1").sendTextToFlash(arr_text, arr_text2);
        }
        function resetForm() {

            getFlashMovie("FlashControl1").resetForm();
            document.getElementById('ucat').value = "";
            document.getElementById("hd").checked = "";
            document.getElementById("vt").checked = "";
            document.getElementById("vm").checked = "";
            document.getElementById("nm").checked = "";

            document.getElementById("extrAanvrButton").style.visibility = "hidden";
        }

        function vulForm(id) {

            ExecuteCall('getAanvraagRecord.aspx', id);

            document.getElementById('extraId').value = id;
        }

        function save() {

            var type;
            if (document.getElementById('hd').checked == true) {
                type = 1;
            }
            else if (document.getElementById('vm').checked == true) {
                type = 2;
            }
            else if (document.getElementById('nm').checked == true) {
                type = 3;
            }
            else {
                type = 0;
            }
            //alert('opslaan.aspx'+ document.getElementById('txtVan').value+ document.getElementById('txtTot').value+ document.getElementById('ucat').value+ type+ document.getElementById('van').value+ document.getElementById('tot').value);
            ExecuteCallSave('opslaan.aspx', document.getElementById('txtVan').value, document.getElementById('txtTot').value, document.getElementById('ucat').value, type, document.getElementById('van').value, document.getElementById('tot').value);
        }

        function enableControls() {
            //  ENABLE CONTROLS

            document.getElementById("txtVan").disabled = "";
            document.getElementById("txtTot").disabled = "";
            document.getElementById("hd").disabled = "";
            document.getElementById("vt").disabled = "";
            document.getElementById("vm").disabled = "";
            document.getElementById("nm").disabled = "";
            document.getElementById("opslaan").disabled = "";
            document.getElementById("reset").disabled = "";
            document.getElementById("van").disabled = "";
            document.getElementById("tot").disabled = "";
        }
        /////////////////////
        // BEGIN AJAX      //
        /////////////////////
        function GetMSXmlHttp() {
            xmlHttp = null;
            try { xmlHttp = new ActiveXObject("Msxml2.XMLHTTP") }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP")
                }
                catch (e) {
                    try {
                        xmlHttp = new XMLHttpRequest()
                    }
                    catch (e) {
                        Alert("Your Browser Does Not Support AJAX")
                    }
                }
            }

            return xmlHttp;
        }
        function SendXmlHttpRequest(xmlhttp, url, id) {
            xmlhttp.open('POST', url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send('id=' + id);
        }
        var xmlHttp;

        function ExecuteCall(url, id) {
            try {
                xmlHttp = GetXmlHttpObject(CallbackMethod);
                SendXmlHttpRequest(xmlHttp, url, id);
            }
            catch (e) { }
        }
        function SendXmlHttpRequestSave(xmlhttp, url, beginDag, eindDag, ucat, type, van, tot) {

            xmlhttp.open('POST', url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send('beginDag=' + beginDag + "&eindDag=" + eindDag + "&ucat=" + ucat + "&type=" + type + "&van=" + van + "&tot=" + tot);
        }


        function ExecuteCallSave(url, beginDag, eindDag, ucat, type, van, tot) {
            document.getElementById("hd").checked = "";
            document.getElementById("vt").checked = "";
            document.getElementById("vm").checked = "";
            document.getElementById("nm").checked = "";
            try {
                xmlHttp = GetXmlHttpObject(CallbackMethod);
                SendXmlHttpRequestSave(xmlHttp, url, beginDag, eindDag, ucat, type, van, tot);
            }
            catch (e) { }
            if ((ucat == 45) || (ucat == 46))
                window.open("openPdf.aspx");
        }
        function CreateXmlHttp(clsid) {
            var xmlHttp = null;
            try {
                xmlHttp = new ActiveXObject(clsid);
                lastclsid = clsid;
                return xmlHttp;
            }
            catch (e) { }
        }
        function GetXmlHttpObject(handler) {

            var objXmlHttp = null;
            if (navigator.appName == "Microsoft Internet Explorer") {

                // Microsoft
                objXmlHttp = GetMSXmlHttp();
                if (objXmlHttp != null) {
                    objXmlHttp.onreadystatechange = handler;
                }
            }
            else {
                // Mozilla | Netscape | Safari
                objXmlHttp = new XMLHttpRequest();
                if (objXmlHttp != null) {
                    objXmlHttp.onload = handler;
                    objXmlHttp.onerror = handler;
                }
            }
            return objXmlHttp;
        }
        function disableControls(enkelDatum) {
            //  DISABLE CONTROLS
            document.getElementById("txtVan").disabled = "disabled";
            document.getElementById("txtTot").disabled = "disabled";
            if (enkelDatum == 0) {
                document.getElementById("hd").disabled = "disabled";
                document.getElementById("vt").disabled = "disabled";
                document.getElementById("vm").disabled = "disabled";
                document.getElementById("nm").disabled = "disabled";
                document.getElementById("opslaan").disabled = "disabled";
                document.getElementById("reset").disabled = "disabled";
                document.getElementById("van").disabled = "disabled";
                document.getElementById("tot").disabled = "disabled";
            }
        }
        function CallbackMethod() {
            try {

                if ((xmlHttp.readyState == 4) || (xmlHttp.readyState == 'complete')) {
                    var response = xmlHttp.responseText;
                    if (response.length > 0) {
                        //update page, debug info
                        if (response.indexOf(";") < 0) {
                            alert(response);
                        }

                        var arr_aanvr = response.split(";");

                        disableControls('0');

                        document.getElementById("txtVan").value = arr_aanvr[0];
                        document.getElementById("txtTot").value = arr_aanvr[1];
                        if (arr_aanvr[2] == 0) {
                            document.getElementById("van").value = arr_aanvr[5];
                            document.getElementById("tot").value = arr_aanvr[6];
                            document.getElementById("vt").checked = true;
                        }
                        else if (arr_aanvr[2] == 1) {
                            document.getElementById("hd").checked = true;
                        }
                        else if (arr_aanvr[2] == 2) {
                            document.getElementById("vm").checked = true;
                        }
                        else if (arr_aanvr[2] == 3) {
                            document.getElementById("nm").checked = true;
                        }
                        document.getElementById("uc" + arr_aanvr[7]).checked = "checked";
                        document.getElementById("ucat").value = arr_aanvr[7];

                        if (arr_aanvr[8] == 0) {

                            document.getElementById("extrAanvrButton").style.visibility = "visible";
                        }
                        else {
                            document.getElementById("extrAanvrButton").style.visibility = "hidden";
                        }
                    }
                    else {
                        document.location.href = "WebForm1.aspx";
                    }

                }
            }
            catch (e) { }
        }
        /////////////////////
        // EIND AJAX      //
        ////////////////////
        function message() {
            if (document.getElementById('change').value == 1) {
                return confirm("Er is een aanpassing gemaakt die nog niet is opgeslaan. Weet u zeker dat u wil afsluiten?");
            }
            else {
                return true;
            }
        }
    </script>

    <style type="text/css">
        body
        {
        	font-family:Trebuchet MS;
        }
    </style>
</head>
<body style="overflow:hidden;background-color:#cccccc;" onload="var today = new Date();document.getElementById('datum').value=today.getFullYear();document.getElementById('flashkader').style.width = '60%';document.getElementById('options').style.width='35%';document.getElementById('flashkader').style.height=screen.width*0.4">

    <form id="form1" name="form1" runat="server">
    <!--<img src="logo.jpg" width="200px" />-->
    <div id="flashkader" style="float:left;width:50%;">
    
        <Bewise:FlashControl ID="FlashControl1" runat="server"
            MovieUrl="~/probeer3.swf" Width="100%" Height="100%" />
    </div>
    <div id="options" style="overflow:scroll;position:absolute;top:0;right:0; height:100%; width:425px; border: 1px solid black; padding:5px;">
        <b><u>Geselecteerd jaar</u></b><br /><br />
        <input type='button' value='<' onclick="var today = new Date();if(parseInt(document.getElementById('datum').value)>today.getFullYear()-1){document.getElementById('datum').value = parseInt(document.getElementById('datum').value)-1;getFlashMovie('FlashControl1').geefJaar(document.getElementById('datum').value)}" /><input type='text' id='datum' name='datum' value='' onchange='var today = new Date();if((parseInt(this.value)>=today.getFullYear()-1)||parseInt(this.value)<=today.getFullYear()+1)){getFlashMovie("FlashControl1").geefJaar(this.value)}' /><input type='button' value='>' onclick="var today = new Date();if(parseInt(document.getElementById('datum').value)<today.getFullYear()+1){document.getElementById('datum').value = parseInt(document.getElementById('datum').value)+1;getFlashMovie('FlashControl1').geefJaar(document.getElementById('datum').value)}" />
        <hr />
        <b><u>Periode afwezigheid</u></b><br /><br />
        Van <input type="text" id="txtVan" onchange="formSend();document.getElementById('change').value='1';" /> Tot
        <input type="text" id="txtTot" onchange="formSend();document.getElementById('change').value='1';" /><br /><hr />
        <b><u>Tijdstip</u></b><br />
        <br />
        
        <input type="radio" id="hd" name="wanneer" value="hd" onfocus="document.getElementById('van').disabled=true;document.getElementById('tot').disabled=true;document.getElementById('change').value='1';" />Hele dag<br />
        <input type="radio" id="vm" name="wanneer" value="vm" onfocus="document.getElementById('van').disabled=true;document.getElementById('tot').disabled=true;document.getElementById('change').value='1';" />Voormiddag<br />
        <input type="radio" id="nm" name="wanneer" value="nm" onfocus="document.getElementById('van').disabled=true;document.getElementById('tot').disabled=true;document.getElementById('change').value='1';" />Namiddag<br />
        <input type="radio" id="vt" name="wanneer" value="u" onfocus="document.getElementById('van').disabled=false;document.getElementById('tot').disabled=false;document.getElementById('change').value='1';" />Van
        <input type="text" id="van" disabled="disabled" size="4" onchange="document.getElementById('change').value='1';" /> 
        Tot&nbsp;<input type="text" id="tot" disabled="disabled" size="4" onchange="document.getElementById('change').value='1';" /><br />
        <hr />
        <b><u>Opname van</u></b><br /><br />
        <div runat="server" id="ucats">
        </div>
        
        <input type="button" id="opslaan" value="Opslaan" onclick="save();resetForm();document.getElementById('change').value='0';" /><input id="reset" type="reset" value="Annuleren" onclick="resetForm();document.getElementById('change').value='0';return false;" />
        
        <asp:Button runat="server" Text="Afsluiten" id="afsluit" 
            onclick="afsluit_Click1" /><br /><br />
            <div style='background-color:  #00B0F0'> Verlof in aanvraag</div>
            <div style='background-color:  #FFE36D'> Verlof in wacht</div>
            <div style='background-color:  #FF0000'> Verlof werd geweigerd</div>
            <input type="button" style="visibility:hidden" runat="server" value="Extra aanvraag toevoegen" id="extrAanvrButton" onclick="javascript: window.open('extrAanvr.aspx?id='+document.getElementById('extraId').value)" />
        <input type="hidden" id="change" /><input type="hidden" id="extraId"  />
    </div>
    </form>
    <div style="position: absolute; left: 100px; bottom: 50px; color: red">
        <b>LET OP: Openstaande aanvragen worden nog niet in uw saldo verrekend!</b>
    </div>
</body>
</html>
