﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Administratie.aspx.cs" Inherits="WebApplication8.Administratie" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

<asp:DropDownList ID="ddKp" DataSourceID="dsKp" 
AutoPostBack="true" DataValueField="kp" runat="server" Width="130px" Font-Size="11px" 
AppendDataBoundItems="true">
    <asp:ListItem Text="All" Value="%"></asp:ListItem>
</asp:DropDownList>
<asp:SqlDataSource ID="dsKp" runat="server" 
ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" SelectCommand="SELECT 
DISTINCT kp from [werknemer]"></asp:SqlDataSource>


        <asp:GridView Width="100%" ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="salnum" DataSourceID="SqlDataSource1" CellPadding="4" 
            ForeColor="#333333" GridLines="None" AllowSorting="True" 
            AllowPaging="True">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:BoundField DataField="salnum" HeaderText="salnum" ReadOnly="True" 
                    SortExpression="salnum" />
                <asp:BoundField DataField="naam" HeaderText="naam" SortExpression="naam" ReadOnly="True" />
                <asp:TemplateField HeaderText="type" SortExpression="type">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" SelectedValue='<%# Bind("type") %>' runat="server" DataValueField="status"   DataTextField="status"  AppendDataBoundItems="true">
                        <asp:ListItem Value="" Text="Gebruiker" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="adm" Text="Administratie" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="per" Text="Annulatie" Enabled="true"></asp:ListItem>
                        <asp:ListItem Value="chf" Text="Chef" Enabled="true"></asp:ListItem>
                        
                    </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="kp" HeaderText="kp" SortExpression="kp" ReadOnly="True" />
                <asp:BoundField DataField="afd" HeaderText="afd" SortExpression="afd" ReadOnly="True" />
                <asp:BoundField DataField="badgenr" HeaderText="badgenr" ReadOnly="True" 
                    SortExpression="badgenr" />
                <asp:BoundField DataField="mail" HeaderText="Email" ReadOnly="False" 
                    SortExpression="mail" />
            </Columns>
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" 
            DeleteCommand="DELETE FROM [werknemer] WHERE [salnum] = @original_salnum AND [naam] = @original_naam AND [type] = @original_type AND [kp] = @original_kp AND [afd] = @original_afd AND [badgenr] = @original_badgenr" 
            InsertCommand="INSERT INTO [werknemer] ([salnum], [naam], [type], [kp], [afd], [badgenr],[mail]) VALUES (@salnum, @naam, @type, @kp, @afd, @badgen,@mailr)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT [salnum], [naam], [type], [kp], [afd], [badgenr], [mail] FROM [werknemer]" 
            FilterExpression="kp like '{0}'"
            UpdateCommand="UPDATE [werknemer] SET [type] = @type,[mail]=@mail WHERE [salnum] = @original_salnum">
            <DeleteParameters>
                <asp:Parameter Name="original_salnum" Type="Int32" />
                <asp:Parameter Name="original_naam" Type="String" />
                <asp:Parameter Name="original_type" Type="String" />
                <asp:Parameter Name="original_kp" Type="String" />
                <asp:Parameter Name="original_afd" Type="String" />
                <asp:Parameter Name="original_badgenr" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="mail" />
                <asp:Parameter Name="original_salnum" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="salnum" Type="Int32" />
                <asp:Parameter Name="naam" Type="String" />
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="kp" Type="String" />
                <asp:Parameter Name="afd" Type="String" />
                <asp:Parameter Name="badgen" />
                <asp:Parameter Name="mailr" />
            </InsertParameters>
            <FilterParameters>
                <asp:ControlParameter Name="kp" ControlID="ddKp" 
                    PropertyName="SelectedValue" />
            </FilterParameters>

        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
