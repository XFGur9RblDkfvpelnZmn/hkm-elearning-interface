﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class print : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = createConnection();
            SqlCommand selectCmd = new SqlCommand("SELECT substring(MIN(aanvraagDetail.datum),7,2) +'/'+substring(MIN(aanvraagDetail.datum),5,2) +'/'+substring(MIN(aanvraagDetail.datum),1,4) AS Begindag, substring(MAX(aanvraagDetail.datum),7,2) +'/'+substring(MAX(aanvraagDetail.datum),5,2) +'/'+substring(MAX(aanvraagDetail.datum),1,4) AS Einddag, substring(aanvraagDetail.van,1,2)+':'+substring(aanvraagDetail.van,3,2) AS 'van', substring(aanvraagDetail.tot,1,2)+':'+substring(aanvraagDetail.tot,3,2) AS 'tot', status = CASE WHEN status = 0 THEN 'IN AANVRAAG' WHEN status=1 THEN 'GOEDGEKEURD' END , type = case WHEN aanvraagDetail.type=1 THEN 'HELE DAG' WHEN aanvraagDetail.type=2 THEN 'VOORMIDDAG' END,omschrijving 'ucat' FROM aanvraag INNER JOIN aanvraagDetail ON aanvraag.id = aanvraagDetail.aanvraagId INNER JOIN ucat ON aanvraag.ucatId = ucat.id WHERE werknemerSalnum = '" + Session["salnum"] + "' GROUP BY aanvraagDetail.van, aanvraagDetail.tot, aanvraagDetail.status, aanvraagDetail.type, omschrijving ", con);
           
            SqlDataReader dr;
            dr = selectCmd.ExecuteReader();
            GridView1.DataSource = dr;
            GridView1.DataBind();
            closeConnection(con);

        }
        //  SLUIT CONNECTIES
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
    }
}
