﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WebApplication8.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HHopla 1.0</title>
</head>

<body onload="document.getElementById('pass').focus()">
    <form id="form1" runat="server" action="login.aspx">
    <div>
        <asp:TextBox AutoPostBack="true" textmode="Password" ID="pass" runat="server" 
            ontextchanged="pass_TextChanged" />
        <input type="submit" value="Aanmelden" />
    </div>
    </form>
</body>
</html>
